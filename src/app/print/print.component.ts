import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-print',
  templateUrl: './print.component.html',
  styleUrls: ['./print.component.css']
})
export class PrintComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }


  print(componentID: string) {

    const printContent = document.getElementById(componentID);
    window.print();
  }
}
