import { Component, OnInit, AfterViewInit } from '@angular/core';

declare var M: any;

declare var $: any;


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'lascanarias-front';

    ngOnInit(): void {
      this.initMaterialize();

      $(document).ready(function(){
        $('.sidenav').sidenav();
      });
    }

    ngAfterViewInit(){
      this.initMaterialize();
    }


   initMaterialize(): void {

    M.AutoInit();
  }
}
