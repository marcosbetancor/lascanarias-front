import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public saludo = '';

  constructor() { }

  ngOnInit() {

    this.getSaludo();
  }


  getSaludo(){

    const today = new Date();
    const curHr = today.getHours();

    if (curHr < 12) {
      this.saludo = 'Buen día';
    } else if (curHr < 18) {
      this.saludo = 'Buenas tardes';
    } else {
      this.saludo = 'Buenas noches';
    }

  }

}
