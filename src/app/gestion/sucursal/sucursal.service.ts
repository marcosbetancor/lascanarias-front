import { Injectable } from '@angular/core';
import { Sucursal } from './sucursal';
import { GenericCrudService } from '../genericcrud/generic-crud.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SucursalService extends GenericCrudService<Sucursal, number> {

  constructor(http: HttpClient) {

    super(environment.baseUrl + '/secured/sucursales', http);
  }
}
