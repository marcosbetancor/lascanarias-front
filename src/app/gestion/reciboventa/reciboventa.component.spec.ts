import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReciboventaComponent } from './reciboventa.component';

describe('ReciboventaComponent', () => {
  let component: ReciboventaComponent;
  let fixture: ComponentFixture<ReciboventaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReciboventaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReciboventaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
