import { Component, OnInit, Input, Output } from '@angular/core';
import { ReciboventaService } from './reciboventa.service';
import { ReciboVenta } from './reciboventa';
import { ActivatedRoute } from '@angular/router';
import { Venta } from '../venta/venta';
import { Recibo } from '../recibo/recibo';
import { Location } from '@angular/common';
declare var M: any;

@Component({
  selector: 'app-reciboventa',
  templateUrl: './reciboventa.component.html',
  styleUrls: ['./reciboventa.component.css']
})
export class ReciboventaComponent implements OnInit {

  public recibo: Recibo;
  public reciboventa = new ReciboVenta();
  public resultado = true;
  public importedesc = 0.0;

  @Input() public ventaseleccionada: Venta;

  @Input() public recibos: Recibo[];

  constructor(public reciboventaservice: ReciboventaService,
    public route: ActivatedRoute,
    public location: Location) { }

  ngOnInit() { }


  onSubmit() {

    if (this.reciboventa.importedesc > this.ventaseleccionada.importerestante) {

      this.resultado = confirm("El importe seleccionado $" + this.reciboventa.importedesc +
        " es mayor que el importe restante en la venta #" +
        this.ventaseleccionada.id.id + " ($" + this.ventaseleccionada.importerestante + "). Confirma?");

      if (this.resultado) {
        this.reciboventa.importedesc = this.ventaseleccionada.importerestante;
      } 
    }

    if(this.reciboventa.importedesc <= 0){
      this.resultado = false;
      M.toast({ html: 'No se puede descontar $0', class: 'red'});
    }

    if(this.reciboventa.importedesc > this.reciboventa.recibo.importerestante){
      this.resultado = false;
      M.toast({ html: "El importe a descontar supera lo disponible en el Recibo #" + this.reciboventa.recibo.id.id +
      " (Imp. restante: $" + this.reciboventa.recibo.importerestante + ")", class: 'red'});
    }

    if (this.resultado) {
      this.reciboventa.venta = this.ventaseleccionada;
      this.reciboventa.venta.entregas = null;

      this.reciboventaservice.save(this.reciboventa).subscribe(
        response => M.toast({ html: 'Recibo aplicado', class: 'green'}),
        (err) => { M.toast({ html: 'No se pudo aplicar recibo: ' + err}); },
        () => window.location.reload());
    }
  }
}
