import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ReciboVenta } from './reciboventa';
import { GenericCrudService } from '../genericcrud/generic-crud.service';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Recibo } from '../recibo/recibo';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ReciboventaService extends GenericCrudService<ReciboVenta, number> {

  constructor(http: HttpClient) {
    super(environment.baseUrl + '/secured/recibosventas', http);
  }

  desaplicar(rv: ReciboVenta): Observable<ReciboVenta> {

    return this.http.post<ReciboVenta>(this.base + '/desaplicar', rv, httpOptions);
  }

}
