import { Sucursal } from "../users/sucursal";

export class ReciboVentaID{

    id: number;
    sucursal: Sucursal;
}