import { ReciboVentaID } from "./reciboventaid";
import { Recibo } from "../recibo/recibo";
import { Venta } from "../venta/venta";

export class ReciboVenta{

    id: ReciboVentaID;
    recibo: Recibo;
    venta: Venta;
    importedesc: number;
    created_at: Date;
    updated_at: Date;

}