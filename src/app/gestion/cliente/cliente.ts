import { Venta } from '../venta/venta';
import { Telefono } from './telefono';
import { TipoVenta } from '../venta/tipoventa';

export class Cliente {

    id: number;
    descrip: string;
    ventas: Venta[];
    telefonos: Telefono[];
    domicilio: String;
    mail: string;
    latitud: string;
    longitud: string;
    content: any;
    tipoventa: TipoVenta;
    active: boolean;
}
