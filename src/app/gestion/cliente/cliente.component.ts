import { Component, OnInit } from '@angular/core';
import { TipoVentaService } from '../venta/tipoventa.service';
import { Cliente } from './cliente';
import { TipoVenta } from '../venta/tipoventa';
import { ClienteService } from './cliente.service';
declare var $: any;
declare var M: any;

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {

  public cliente = new Cliente();
  public tipoventas$: TipoVenta[];

  constructor(public tipoventaservice: TipoVentaService,
              public clienteservice: ClienteService) { }

  ngOnInit() {

    this.getTipoVentas();
  }

  getTipoVentas() {
    this.tipoventaservice.findAll().subscribe(tipoventas => this.tipoventas$ = tipoventas,
      error =>  M.toast({ html: error, class: 'red'}));
  }

  submit() {

    
    $('#btncrear').hide();

    this.clienteservice.save(this.cliente).subscribe(data => {

      M.toast({ html: 'Cliente creado', class: 'green'});
      window.location.reload();

    },
      error => { 
        M.toast({ html: 'No se pudo crear el cliente: ' + error, class: 'red'});
        $('#btncrear').show();
      }
      ,
      () => M.toast({ html: 'Cliente creado', class: 'green'}));

  }
}
