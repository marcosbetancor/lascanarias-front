import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../cliente.service';
import { Cliente } from '../cliente';
import { TipoVenta } from '../../venta/tipoventa';
import { DeviceDetectorService } from 'ngx-device-detector';

declare var $: any;

@Component({
  selector: 'app-listadocliente',
  templateUrl: './listadocliente.component.html',
  styleUrls: ['./listadocliente.component.css']
})
export class ListadoclienteComponent implements OnInit {

  public clientes: Cliente[];
  public cliente = new Cliente();
  clienteanular: Cliente;
  palabraanular = '';
  public isMobile: boolean;

  // PAGINADOR
  public page = 0; // Número de página en la que estamos. Será 1 la primera vez que se carga el componente
  public totalPages: Number; // Número total de páginas
  public numItems: Number; // Total de items existentes
  public size = 25;
  public order = 'descrip';
  public asc = true;
  public isFirst = false;
  public isLast = false;
  public search = '';
  public tipoventa = TipoVenta;
  public active = true;

  // CIERRA PAGINADOR

  constructor(public clienteservice: ClienteService,) { }

  ngOnInit() {

    this.findAllClientes();

    $(document).ready(function () {
      
      $('.collapsible').collapsible();
      //$('a > .tooltipped').tooltip();
      $('.modal').modal();
      $('.dropdown-trigger').dropdown();
    });

  }

  findAllClientes() {

    this.clienteservice.findAllByPage(this.search, this.page, this.size, this.order, this.asc, this.active).subscribe(data => {

      this.clientes = data.content;
      this.isFirst = data.first;
      this.isLast = data.last;
      this.totalPages = data.totalPages;
      this.numItems = data.numberOfElements;

      $(document).ready(function () {
      
        $('.dropdown-trigger').dropdown();
      });

    },
      err => {
       //alert(err.error);
      }
    );

  }


  goToPage(page: number) {

    this.page = page;

    this.findAllClientes();

  }


  searchFn() {

    this.search = '';

    if (this.cliente != null) {

      if(this.cliente.descrip.length >= 3){
        this.search = this.cliente.descrip;
        this.page = 0;
        this.findAllClientes();
     }
    }

}

anularModal(c: Cliente){

  this.clienteanular = c;
  
  $('.modal').modal('open');

}

anular() {

      if(this.palabraanular === 'ANULAR'){
      this.clienteservice.delete(this.clienteanular.id).subscribe(data => {

        alert('Cliente deshabilitado correctamente');
        window.location.reload();

      },
       error =>  {
         alert('Error al deshabilitar cliente: ' + error);
         window.location.reload(); }
       );
      } else {
        $('.modal').modal('close');
        alert('Anulacion del cliente cancelada');
        this.palabraanular = '';
      }
    }
  }
