import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Cliente } from './cliente';
import { GenericCrudService } from '../genericcrud/generic-crud.service';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type':'application/json; charset=utf-8'})
};
const httpOptionsPlain = {
  headers: new HttpHeaders({'Content-Type': 'text/plain; charset=utf-8'})
};



@Injectable({
  providedIn: 'root'
})
export class ClienteService extends GenericCrudService<Cliente, number> {

  constructor(http: HttpClient) {

    super(environment.baseUrl + '/secured/clientes', http);
 }

 findAllSinPage(): Observable<Cliente[]> {
  return this.http.get<Cliente[]>(this.base + '/withoutpage', httpOptions);
}

calcularDeuda(idcliente: number): Observable<number> {


  return this.http.post<number>(this.base + '/clientedeuda', idcliente, httpOptionsPlain);
}

}
