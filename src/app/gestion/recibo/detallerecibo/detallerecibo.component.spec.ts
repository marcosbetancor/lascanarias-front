import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallereciboComponent } from './detallerecibo.component';

describe('DetallereciboComponent', () => {
  let component: DetallereciboComponent;
  let fixture: ComponentFixture<DetallereciboComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallereciboComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallereciboComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
