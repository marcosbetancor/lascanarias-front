import { Component, OnInit } from '@angular/core';
import { ReciboService } from '../recibo.service';
import { ActivatedRoute } from '@angular/router';
import { Recibo } from '../recibo';

@Component({
  selector: 'app-detallerecibo',
  templateUrl: './detallerecibo.component.html',
  styleUrls: ['./detallerecibo.component.css']
})
export class DetallereciboComponent implements OnInit {

  public recibo = new Recibo();

  constructor(public reciboservice: ReciboService,
              public route: ActivatedRoute) { }

  ngOnInit() {
    this.getRecibo();
  }

  getRecibo() {

    const id = +this.route.snapshot.paramMap.get('id');
    const sucursal = +this.route.snapshot.paramMap.get('sucursal');

    if (id && sucursal) {
      this.reciboservice.findOneByIdAndSucursal(id, sucursal).subscribe(recibo => {
        this.recibo = recibo;
      },
        error => alert(error));
    }

  }

}
