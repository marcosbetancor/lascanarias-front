import { Component, OnInit } from '@angular/core';
import { ReciboService } from './recibo.service';
import { Recibo } from './recibo';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Cliente } from '../cliente/cliente';
import { ClienteService } from '../cliente/cliente.service';
declare var $: any;
declare var M: any;

@Component({
  selector: 'app-recibo',
  templateUrl: './recibo.component.html',
  styleUrls: ['./recibo.component.css']
})
export class ReciboComponent implements OnInit {

    //PAGINADOR
 public page = 0; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente
 public totalPages: Number; //Número total de páginas
 public numItems: Number; //Total de items existentes
 public size = 10000;
 public order = 'id';
 public asc = false;
 public isFirst = false;
 public isLast = false;
 public search = '';
 loading = false;

 // CIERRA PAGINADOR

  public recibo = new Recibo();
  public clientes$: Cliente[];
  public clienteselect: Cliente;

  constructor(public reciboservice: ReciboService,
              public clienteservice: ClienteService,
              public location: Location,
              public route: ActivatedRoute) { }

  ngOnInit() {

    this.getAllClientes();
  }
  
  getAllClientes() {
    
    this.clienteservice.findAllByPage(this.search, this.page, this.size, this.order, this.asc, true)
      .subscribe(clientes => this.clientes$ =  clientes.content);
  }

  
  guardarRecibo(): void {

    $('.btncrear').hide();
      this.reciboservice.save(this.recibo).subscribe(
        response => M.toast({ html: 'Recibo creado correctamente', class: 'green'}),
        error => M.toast({ html: 'No se pudo crear el recibo: ' + error, class: 'red'}),
        () => this.goBack());
        
    $('.btncrear').show();
      }

  
  goBack(): void {
    window.location.reload();
  }


}
