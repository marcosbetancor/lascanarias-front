
import { ReciboID } from "./reciboID";
import { Cliente } from "../cliente/cliente";

export class Recibo{

    id: ReciboID;
    created_at: Date;
    updated_at: Date;
    importe: Number;
    importerestante: Number;
    cliente: Cliente;
    descripadic: String;
    active: boolean;
}