import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoreciboComponent } from './listadorecibo.component';

describe('ListadoreciboComponent', () => {
  let component: ListadoreciboComponent;
  let fixture: ComponentFixture<ListadoreciboComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListadoreciboComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoreciboComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
