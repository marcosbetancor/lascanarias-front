import { Component, OnInit } from '@angular/core';
import { Recibo } from '../recibo';
import { Sucursal } from '../../users/sucursal';
import { Cliente } from '../../cliente/cliente';
import { ReciboService } from '../recibo.service';
import { SucursalService } from '../../sucursal/sucursal.service';
import { ClienteService } from '../../cliente/cliente.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ReciboVenta } from '../../reciboventa/reciboventa';
import { ReciboventaService } from '../../reciboventa/reciboventa.service';
declare var $: any;

@Component({
  selector: 'app-listadorecibo',
  templateUrl: './listadorecibo.component.html',
  styleUrls: ['./listadorecibo.component.css']
})
export class ListadoreciboComponent implements OnInit {

  //PAGINADOR
  public page = 0; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente
  public totalPages: Number; //Número total de páginas
  public numItems: Number; //Total de items existentes
  public size = 25;
  public order = 'id.id';
  public asc = false;
  public isFirst = false;
  public isLast = false;
  public search = '';

  // CIERRA PAGINADOR

  public id: number;
  public recibosventas$: ReciboVenta[];
  public recibos$: Recibo[];
  public sucursales$: Sucursal[];
  public sucursalselect: Sucursal;
  public clientes$: Cliente[];
  public clienteselect: Cliente;
  public idcliente = null;

  constructor(
    public reciboservice: ReciboService,
    public route: ActivatedRoute,
    public location: Location) { }

  ngOnInit() {

    this.getAllRecibosByPage();

    $(document).ready(function () {
      $('.collapsible').collapsible();
    });
  }

  goToPage(page: number) {

    this.page = page;

    this.getAllRecibosByPage();

  }

  getAllRecibosByPage() {

    this.reciboservice.findAllByPage(this.search, this.page, this.size, this.order, this.asc).subscribe(data => {

      this.recibos$ = data.content;
      this.isFirst = data.first;
      this.isLast = data.last;
      this.totalPages = data.totalPages;
      this.numItems = data.numberOfElements;
    },
      err => {
        console.log(err.error);
      }
    );

  }

    goBack(): void {
      window.location.reload();
    }

    anular(id: number, sucursal: number){

      let flag = false;
      flag = confirm('¿Esta seguro que desea eliminar el recibo #?' + id);

      if (flag) {

        this.reciboservice.anularRecibo(id, sucursal).subscribe(() => this.goBack());

      }
    }

    
  searchFn() {

    this.search = '';

    if(this.id != null && this.sucursalselect != null ){

      this.search += 'id.id==' + this.id + ';id.sucursal.id==' + this.sucursalselect.id;
      this.page = 0;
      this.id = null;
      this.sucursalselect = null;
      this.getAllRecibosByPage();
    }

    if (this.id != null) {

      this.search += 'id.id==' + this.id;
      this.page = 0;
      this.id = null;
      this.getAllRecibosByPage();

    } 

    if (this.sucursalselect != null) {

      this.search += 'id.sucursal.id==' + this.sucursalselect.id;
      this.page = 0;
      this.id = null;
      this.getAllRecibosByPage();
    }

    if(this.clienteselect != null){

      this.search += 'cliente.id==' + this.clienteselect.id;
      this.page = 0;
      this.id = null;
      this.getAllRecibosByPage();
    }

    this.getAllRecibosByPage();

    }

}
