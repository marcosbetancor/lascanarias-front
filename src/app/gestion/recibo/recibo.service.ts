import { Injectable } from '@angular/core';
import { Recibo } from './recibo';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GenericCrudService } from '../genericcrud/generic-crud.service';
import { environment } from 'src/environments/environment';
import { ReciboVenta } from '../reciboventa/reciboventa';

import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ReciboService extends GenericCrudService<Recibo, number> {
  
  constructor(http: HttpClient) {
    super(environment.baseUrl + '/secured/recibos', http);
 }

}