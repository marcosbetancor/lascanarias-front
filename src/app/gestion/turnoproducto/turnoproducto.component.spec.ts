import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TurnoproductoComponent } from './turnoproducto.component';

describe('TurnoproductoComponent', () => {
  let component: TurnoproductoComponent;
  let fixture: ComponentFixture<TurnoproductoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TurnoproductoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TurnoproductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
