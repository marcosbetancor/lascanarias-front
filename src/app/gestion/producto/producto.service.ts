import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Producto } from './producto';
import { GenericCrudService } from '../genericcrud/generic-crud.service';
import { environment } from 'src/environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class ProductoService extends GenericCrudService<Producto, number> {

 constructor(http: HttpClient) {

  super(environment.baseUrl + '/productos', http);
}

}
