import {TipoProducto} from './tipoproducto/tipoproducto';

export class Producto {

    id: number;
    descrip: string;
    precio: number;
    tipoproducto: TipoProducto;
    active: boolean;
    listapub: boolean;
    abrev: string;
    cantporpallet: number;
 
    constructor(tipoproducto: TipoProducto) {
        this.tipoproducto = tipoproducto;
    }

}
