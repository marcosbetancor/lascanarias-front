import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductoComponent } from './producto.component';
import { ProductoDetailComponent } from './producto-detail/producto-detail.component';
import { TipoproductoComponent } from './tipoproducto/tipoproducto.component';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../../app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { PipesModule } from '../../pipes/pipes.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { LoaderComponent } from 'src/app/loader/loader.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    PipesModule,
    NgSelectModule
  ],
  declarations: [
    ProductoComponent,
    ProductoDetailComponent,
    TipoproductoComponent,
    LoaderComponent
  ]
})
export class ProductoModule { }
