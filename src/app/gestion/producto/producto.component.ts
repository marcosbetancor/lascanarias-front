import { Component, OnInit, Input } from '@angular/core';
import { ProductoService } from './producto.service';
import { Producto } from './producto';
import { TipoproductoService } from './tipoproducto/tipoproducto.service';
import { TipoProducto } from './tipoproducto/tipoproducto';
import { Location } from '@angular/common';
declare var $: any;
declare var M: any;


@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

    @Input() producto: Producto;
    @Input() prod = new Producto(new TipoProducto());
    @Input() productos: Producto[];
    @Input() tipos: TipoProducto[];
    public tiposelect: TipoProducto;
    public searchText: string;
    loading = false;

  constructor(
    public productoService: ProductoService,
    public tipoProductoService: TipoproductoService,
    public location: Location) { }

  ngOnInit() {

    $(document).ready(function () {
      $('.modal').modal();
      $('select').formSelect();
    });

    this.getTipos();
    this.getProductos();
  }

      compare( a, b ) {
        if ( a.descrip < b.descrip ){
          return -1;
        }
        if ( a.descrip > b.descrip ) {
          return 1;
        }
        return 0;
      }
  

     getProductos(): void {

      this.loading = true;

        this.productoService.findAll().subscribe(
          productos => this.productos = productos,
          error => M.toast({ html: 'No se pudo conseguir el listado de productos: ' + error, class: 'red'}),
          () => this.loading = false);

        this.productos = this.productos.sort((a, b) => (a.descrip > b.descrip) ? 1 : -1);
      }

      goBack(): void {
        window.location.reload();
      }

      onSubmit(): void {
        this.productoService.save(this.prod).subscribe(
          response => M.toast({ html: 'Producto creado correctamente', class: 'green'}),
          error => M.toast({ html: 'No se pudo crear el producto', class: 'red'}),
          () => window.location.reload());
      }

      update(producto: Producto): void {

        this.productoService.update(producto.id, producto).subscribe(
          response => M.toast({ html: 'Producto modificado correctamente', class: 'green'}),
          error => M.toast({ html: 'No se pudo modificar el producto', class: 'red'}),
          () => window.location.reload());
      }

      getTipos(): void {

        this.loading = true;

        this.tipoProductoService.findAll().subscribe(tipos => this.tipos = tipos,
          error => console.log('No se pudo conseguir el listado de tipos: ' + error),
          () => this.loading = false);

      }



}
