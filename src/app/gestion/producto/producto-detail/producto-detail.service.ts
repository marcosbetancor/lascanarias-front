import { Injectable } from '@angular/core';
import { GenericCrudService } from '../../genericcrud/generic-crud.service';
import { Producto } from '../producto';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductoDetailService extends GenericCrudService<Producto, number> {

  constructor(http: HttpClient ) {

    super(environment.baseUrl + '/productos', http);

   }
}
