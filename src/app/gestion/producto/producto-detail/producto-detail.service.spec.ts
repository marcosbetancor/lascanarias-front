import { TestBed, inject } from '@angular/core/testing';

import { ProductoDetailService } from './producto-detail.service';

describe('ProductoDetailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductoDetailService]
    });
  });

  it('should be created', inject([ProductoDetailService], (service: ProductoDetailService) => {
    expect(service).toBeTruthy();
  }));
});
