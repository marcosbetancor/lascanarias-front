import { Component, OnInit, Input } from '@angular/core';
import { ProductoDetailService } from './producto-detail.service';
import { Producto } from '../producto';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { TipoProducto } from '../tipoproducto/tipoproducto';
import { TipoproductoService } from '../tipoproducto/tipoproducto.service';
declare var $: any;

@Component({
  selector: 'app-producto-detail',
  templateUrl: './producto-detail.component.html',
  styleUrls: ['./producto-detail.component.css']
})
export class ProductoDetailComponent implements OnInit {

  @Input() producto: Producto;
  @Input() tipos: TipoProducto[];

  constructor(
    public productoDetailService: ProductoDetailService,
    public tipoProductoService: TipoproductoService,
    public route: ActivatedRoute,
    public location: Location) { }

  ngOnInit() {

    this.getProducto();
    this.getTipos();
  }

  getProducto(): void {

    const id = +this.route.snapshot.paramMap.get('id');
    this.productoDetailService.findOne(id).subscribe(producto => this.producto = producto);
  }

  getTipos(): void {

    this.tipoProductoService.findAll().subscribe(tipos => this.tipos = tipos);
  }
  goBack(): void {
    window.location.reload();
  }

  update(producto: Producto): void {

    this.productoDetailService.update(this.producto.id, producto).subscribe(
    response => alert('Producto modificado correctamente'),
     error => alert('No se pudo crear el producto: ' + error),
     () => this.goBack())
    }
}

