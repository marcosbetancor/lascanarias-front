import { Component, OnInit, Input } from '@angular/core';
import { TipoProducto } from './tipoproducto';
import { TipoproductoService } from './tipoproducto.service';
declare var $: any;

@Component({
  selector: 'app-tipoproducto',
  templateUrl: './tipoproducto.component.html',
  styleUrls: ['./tipoproducto.component.css']
})
export class TipoproductoComponent implements OnInit {

  @Input() tipo: TipoProducto;
  tipos: TipoProducto[];


  constructor(public tipoProductoService: TipoproductoService) {}

  ngOnInit() {
    this.getTipos();
    
    $(document).ready(function () {
      $('.modal').modal();
    });
  }

  getTipos(): void {
    this.tipoProductoService.findAll().subscribe(tipos => this.tipos = tipos);
  }

}
