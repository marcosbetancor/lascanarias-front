import { Fabrica } from "../../fabrica/fabrica";

export class TipoProducto{
    id: number;
    descrip: string;
    active: boolean;
    leyenda: string;
    created_at: Date;
    updated_at: Date;
    fabricas: Fabrica[];
}