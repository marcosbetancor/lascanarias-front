import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TipoProducto} from './tipoproducto';
import { GenericCrudService } from '../../genericcrud/generic-crud.service';
import { environment } from 'src/environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class TipoproductoService extends GenericCrudService<TipoProducto, number> {

    constructor(http: HttpClient) {

      super(environment.baseUrl + '/tipoproductos', http);
  }
  }
