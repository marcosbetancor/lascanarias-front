import { Producto } from "../producto/producto";
import { LineaEntregaId } from "./lineaentregaid";

export class LineaEntrega{

    id: LineaEntregaId;
    cantidad: number;
    producto: Producto;
    descripadic: string;
}