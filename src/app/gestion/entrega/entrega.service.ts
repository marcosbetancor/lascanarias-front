import { Injectable } from '@angular/core';
import { GenericCrudService } from '../genericcrud/generic-crud.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Entrega } from './entrega';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Turno } from '../turno/turno';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' ,
  'Access-Control-Allow-Methods': 'POST,GET,PUT,PATCH,DELETE,OPTIONS',
  'Access-Control-Allow-Credentials': 'true'})
};

@Injectable({
  providedIn: 'root'
})
export class EntregaService extends GenericCrudService<Entrega, number> {

  constructor(http: HttpClient) {

    super(environment.baseUrl + '/secured/entregas', http);

   }

   findAllSinMonetizar(cliente: number): Observable<Entrega[]> {
    return this.http.get<Entrega[]>(this.base + '/' + cliente + '/' +  'sinmonetizar', httpOptions);
  }

  generarEntregaDesdeTurno(entrega: Entrega): Observable<Entrega>{

    return this.http.post<Entrega>(this.base + '/entregaturno', entrega, httpOptions);
  }

  updateEntrega(entrega: Entrega): Observable<Entrega>{

    return this.http.put<Entrega>(this.base + '/', entrega, httpOptions);
  }
  }
