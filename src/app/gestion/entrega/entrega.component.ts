import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../cliente/cliente.service';
import { UsersService } from '../users/users.service';
import { EntregaService } from './entrega.service';
import { Entrega } from './entrega';
import { Cliente } from '../cliente/cliente';
import { Producto } from '../producto/producto';
import { LineaEntrega } from './lineaentrega';
import { TipoProducto } from '../producto/tipoproducto/tipoproducto';
import { Vendedor } from '../venta/vendedor';
import { ProductoService } from '../producto/producto.service';
import { VendedorService } from '../venta/vendedor.service';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { TurnoService } from '../turno/turno.service';
import { Turno } from '../turno/turno';
import { isNumeric } from 'rxjs/internal/util/isNumeric';
declare var $: any;
declare var M: any;

@Component({
  selector: 'app-entrega',
  templateUrl: './entrega.component.html',
  styleUrls: ['./entrega.component.css']
})
export class EntregaComponent implements OnInit {

  public producto = new Producto(new TipoProducto());
  public productos$: Producto[];
  public entrega = new Entrega();
  public lineaentrega = new LineaEntrega();
  public total = 0.0;
  public vendedores$: Vendedor[];
  public clientes$: Cliente[];
  public clienteselect: Cliente;
  public date: Date;
  public idcliente;
  public turno = new Turno();
  public id: number;
  public sucursal: number;
  loading = false;

  //PAGINADOR
  public page = 0; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente
  public totalPages: Number; //Número total de páginas
  public numItems: Number; //Total de items existentes
  public size = 1000000;
  public order = 'id';
  public asc = false;
  public isFirst = false;
  public isLast = false;
  public search = '';

  // CIERRA PAGINADOR
  constructor(public productoservice: ProductoService,
    public usuarioservice: UsersService,
    public clienteservice: ClienteService,
    public entregaservice: EntregaService,
    public vendedorservice: VendedorService,
    public turnoservice: TurnoService,
    public location: Location,
    public route: ActivatedRoute,
    public router: Router) {

    this.date = new Date();
  }

  ngOnInit() {


    this.initSelect();
    this.getTurno();
    this.getAllProductos();
    this.getAllVendedores();
    this.getAllClientes();

  }

  
  getTurno() {

    this.loading = true;

    this.id = Number (this.route.snapshot.paramMap.get('idturno'));
    this.sucursal = Number (this.route.snapshot.paramMap.get('sucturno'));

    if (this.id && this.sucursal) {

      this.turnoservice.findOneByIdAndSucursal(this.id, this.sucursal).subscribe(
        turno => {

          this.turno = turno;

            this.clienteservice.findOne(this.turno.destino.id)
            .subscribe(cliente => this.entrega.cliente = cliente);
            this.turno.turnoproductos.forEach(x => {

            this.lineaentrega = new LineaEntrega();
            this.lineaentrega.cantidad = x.cantidad;
            this.lineaentrega.producto = x.producto;

            this.entrega.lineasentregas.push(this.lineaentrega);
            this.lineaentrega = new LineaEntrega();


          });
        },
        () => {this.loading = false;});
    }
  }

  getAllClientes() {

    this.loading = true;
    
    this.clienteservice.findAllByPage(this.search, this.page, this.size, this.order, this.asc, true)
      .subscribe(clientes => this.clientes$ = clientes.content,
        () => { this.loading = false; });
  }
  getAllProductos() {
    this.loading = true;
    this.productoservice.findAll().subscribe(productos => this.productos$ = productos,
      () => { this.loading = false; });
  }

  getAllVendedores() {
    this.loading = true;
    this.vendedorservice.findAll().subscribe(vendedores => {
      this.vendedores$ = vendedores,
      this.hacerNombreApellidoCompleto();
      this.loading = false; });
  }


  agregarLinea() {

    if (this.lineaentrega.producto) {
      if (this.lineaentrega.cantidad != null && this.lineaentrega.cantidad > 0) {
        this.entrega.lineasentregas.push(this.lineaentrega);
        this.lineaentrega = new LineaEntrega();
        $('#cant').focus();

        $(document).ready(function () {
          $('.dropdown-trigger').dropdown();
        });
      } else {
        M.toast({ html: 'La cantidad no es válida', classes: 'red'});
      }
    } else {
      M.toast({ html: 'No se puede agregar sin asignar producto', classes: 'red'});
    }
  }

  hacerNombreApellidoCompleto() {
    let i: Vendedor;
    // tslint:disable-next-line:no-shadowed-variable
    this.vendedores$.forEach(function (i) {
      i.nombreapellido = i.nombre + ' ' + i.apellido;
    }
    );
  }

  guardarEntrega(): void {

    
    $('.btncrear').hide();
    this.loading = true;

    if(this.id > 0 && this.sucursal > 0) {

      this.entrega.turno = this.turno;
      this.entregaservice.generarEntregaDesdeTurno(this.entrega).subscribe(data => {

        this.entrega = data;
        
        M.toast({ html: 'Entrega generada correctamente', classes: 'green'});
        
        this.router.navigate(['/gestion/entregas/' + this.entrega.id.id + '/' + this.entrega.id.sucursal.id]);

    }, err => {
      
      M.toast({ html: err.error, class: 'red'});
    },
    () => this.loading = false);

  } else {


    if(this.entrega.cliente != null){

    if(this.entrega.vendedor != null){
      this.entrega.sucursal = this.entrega.vendedor.sucursal;
      this.entregaservice.save(this.entrega).subscribe( data => {
        
        this.entrega = data;
        
        M.toast({ html: 'Entrega #' + this.entrega.id.id + ' generada correctamente', classes: 'green'});
        
        this.router.navigate(['/gestion/entregas/' + this.entrega.id.id + '/' + this.entrega.id.sucursal.id]);
        
      },
        error => M.toast({ html: 'No se pudo crear la entrega: ' + error, classes: 'red'}),
        () => {
          this.loading = false;
        });

      } else {
        M.toast({ html: 'Debe elegir vendedor', classes: 'red'});
      }
      } else{
        M.toast({ html: 'Debe elegir cliente', classes: 'red'});
      }
    }

    
    $('.btncrear').show();
  }

  initSelect() {
    $(document).ready(function () {
      $('.modal').modal();
      $('.dropdown-trigger').dropdown();
      (<any>$('select')).formSelect();

    });
  }

  goBack(): void {
    window.location.reload();
  }


  eliminarFila(i: number) {

    this.entrega.lineasentregas.splice(i, 1);
  }


  calcularPallets() {

      let cant = 0;
      this.entrega.cantidadpallets = 0;


      this.entrega.lineasentregas.forEach(x => {

        if (!isNaN(x.producto.cantporpallet)) {
          cant += Math.ceil(x.cantidad / x.producto.cantporpallet);
        }
      });

      this.entrega.cantidadpallets = cant;

      alert(JSON.stringify(this.entrega));
      if (this.entrega.cantidadpallets > 0 && this.entrega.cantidadpallets != null) {
        
        $("#modalpallet").modal('open');
      } else {
        
        this.guardarEntrega();
      }
  }

}

