import { Component, OnInit } from '@angular/core';
import { Entrega } from '../entrega';
import { EntregaService } from '../entrega.service';
import { ActivatedRoute, Router } from '@angular/router';
declare var M:any;

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {

  public entrega = new Entrega();

  constructor(public entregaservice: EntregaService,
              public route: ActivatedRoute,
              public router: Router) { }

  ngOnInit() {

    this.getEntrega();
  }

  getEntrega() {

    const id = +this.route.snapshot.paramMap.get('id');
    const sucursal = +this.route.snapshot.paramMap.get('sucursal');

    if (id && sucursal) {
      this.entregaservice.findOneByIdAndSucursal(id, sucursal).subscribe(entrega => {
        this.entrega = entrega;
      },
      
        error => M.toast({ html: error, class: 'red'}));
    }

  }


  /* editableDescrip(){
    const tds = document.getElementsByClassName('editable');
    for(let i = 0; i < tds.length; i++) {
      let td = tds[i];
      td.contentEditable = 'true';
    }

  } */

  guardarDescrip(id: number, value: string){

    this.entrega.lineasentregas.find(x => x.id.id === id).descripadic = value;
    

  }

  modificarEntrega(){

    this.entrega.ventas = [];
    this.entrega.turno = null;

    this.entregaservice.updateEntrega(this.entrega).subscribe( data => {
        
      this.entrega = data;
      M.toast({ html: 'Entrega #' + this.entrega.id.id + ' modificada correctamente', class: 'green'});
      window.location.reload();
      
    },
      error => M.toast({ html: 'No se pudo modificar la entrega: ' + error, class: 'red'}),
      () => {
        window.location.reload();
      });
  }

}
