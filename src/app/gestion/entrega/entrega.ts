import { Cliente } from "../cliente/cliente";
import { EntregaId } from "./entregaid";
import { Vendedor } from "../venta/vendedor";
import { Sucursal } from "../users/sucursal";
import { LineaEntrega } from "./lineaentrega";
import { Venta } from "../venta/venta";
import { Turno } from "../turno/turno";

export class Entrega{

    id: EntregaId;
    cliente: Cliente;
    vendedor: Vendedor;
    sucursal: Sucursal;
    direccion: string;
    telefono: string;
    descripadic: string;
    latitud: string;
    longitud: string;
    lineasentregas: LineaEntrega[] = [];
    seleccionado: boolean;
    turno: Turno;
    created_at: Date;
    active: boolean;
    ventas: Venta[];
    cantidadpallets = 0;

}