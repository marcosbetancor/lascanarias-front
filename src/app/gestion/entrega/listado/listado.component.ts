import { Component, OnInit } from '@angular/core';
import { EntregaService } from '../entrega.service';
import { Entrega } from '../entrega';
import { Sucursal } from '../../users/sucursal';
import { Cliente } from '../../cliente/cliente';
import { ClienteService } from '../../cliente/cliente.service';
import { SucursalService } from '../../sucursal/sucursal.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
declare var $: any;
declare var M: any;

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {


  //PAGINADOR
  public page = 0; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente
  public totalPages: Number; //Número total de páginas
  public numItems: Number; //Total de items existentes
  public size = 25;
  public order = 'id.id';
  public asc = false;
  public isFirst = false;
  public isLast = false;
  public search = '';

  // CIERRA PAGINADOR

  public id: number;
  public idsucursal = null;
  public entregas$: Entrega[];
  public sucursales$: Sucursal[];
  public sucursalselect: Sucursal;
  public clientes$: Cliente[];
  public clienteselect: Cliente;
  public idcliente = null;
  public searchAllclientes = '';
  loading = false;

  constructor(public entregaservice: EntregaService,
              public clienteservice: ClienteService,
              public sucursalservice: SucursalService,
              public route: ActivatedRoute,
              public location: Location,
              public router: Router
              ) { }

  ngOnInit() {

      this.idcliente = this.route.snapshot.paramMap.get('idcliente');
      
      this.idsucursal = this.route.snapshot.paramMap.get('sucursal');
      
      if(this.idcliente){

        this.search = 'cliente.id==' + this.idcliente;
        this.page = 0;
        this.getAllEntregasByPage();
        this.search = '';
     } else {
        this.getAllEntregasByPage();
     }

    this.getAllSucursales();
    this.getAllClientes();

    $(document).ready(function () {
      $('.collapsible').collapsible();
    });
  }

  goToPage(page: number) {

    this.page = page;

    this.getAllEntregasByPage();

  }

  
  getAllEntregasByPage() {

    this.loading = true;
    this.entregaservice.findAllByPage(this.search, this.page, this.size, this.order, this.asc).subscribe(data => {

      this.entregas$ = data.content;
      this.isFirst = data.first;
      this.isLast = data.last;
      this.totalPages = data.totalPages;
      this.numItems = data.numberOfElements;
    },
      err => {
        console.log(err.error);
      },
      () => this.loading = false
    );

  }

  getAllClientes(){

    this.clienteservice.findAllSinPage().subscribe(clientes => this.clientes$ = clientes,
      error => console.log(error));
  }

  getAllSucursales() {

    this.sucursalservice.findAll().subscribe(sucursales => this.sucursales$ = sucursales,
      error => console.log(error));
  }

  goBack(): void {
    window.location.reload();
  }

  searchFn() {

    this.search = '';

    if(this.id != null && this.sucursalselect != null ){

      this.search += 'id==' + this.id;
      this.page = 0;
      this.id = null;
      this.sucursalselect = null;
      this.getAllEntregasByPage();
    }

    if (this.id != null) {

      this.search += 'id.id==' + this.id;
      this.page = 0;
      this.id = null;
      this.getAllEntregasByPage();

    }

    if (this.sucursalselect != null) {

      this.search += 'id.sucursal.id==' + this.sucursalselect.id;
      this.page = 0;
      this.id = null;
      this.getAllEntregasByPage();
    }

    if(this.clienteselect != null){

      this.search += 'cliente.id==' + this.clienteselect.id;
      this.page = 0;
      this.id = null;
      this.getAllEntregasByPage();
    }

    this.getAllEntregasByPage();

    }

    anular(id: number, sucursal: number) {

      
    let flag = false;
    flag = confirm('Esta seguro que desea eliminar la entrega #?' + id);
    if (flag) {
      this.entregaservice.anularEntrega(id, sucursal).subscribe(response => {

        M.toast({ html: 'Entrega anulada', class: 'green'});
        window.location.reload();
      }, error => {M.toast({ html: error.message, class: 'red'}); }
      );
  }
}
}