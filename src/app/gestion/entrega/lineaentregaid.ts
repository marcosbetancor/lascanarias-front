import { Entrega } from "./entrega";

export class LineaEntregaId{
    id: number;
    entrega: Entrega;
}