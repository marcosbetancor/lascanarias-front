import { Component, OnInit } from '@angular/core';
import { EntregaService } from '../entrega/entrega.service';
import { VentaService } from '../venta/venta.service';
import { TurnoService } from '../turno/turno.service';
import { ActivatedRoute } from '@angular/router';
import { Entrega } from '../entrega/entrega';
import { Venta } from '../venta/venta';
import { Location } from '@angular/common';
import { Turno } from '../turno/turno';
import { TurnoId } from '../turno/turnoid';
import { Recibo } from '../recibo/recibo';
import { ReciboService } from '../recibo/recibo.service';
import { ClienteService } from '../cliente/cliente.service';
declare var $: any;
declare var M: any;

@Component({
  selector: 'app-resumendeuda',
  templateUrl: './resumendeuda.component.html',
  styleUrls: ['./resumendeuda.component.css']
})
export class ResumendeudaComponent implements OnInit {

//PAGINADOR
public page = 0; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente
public totalPages: Number; //Número total de páginas
public numItems: Number; //Total de items existentes
public size = 1000;
public order = 'id.id';
public asc = false;
public isFirst = false;
public isLast = false;
public search = '';
public entregas$: Entrega[] = [];
public ventas$: Venta[] = [];
public turnos$: Turno[] = [];
public sizeentregas: number;
public sizeventas: number;
public sizeturnos: number;
public entrega: Entrega;
public recibos$: Recibo[];
public recibosconsaldo$: Recibo[];
public cantrecibos: Boolean;
public ventaseleccionada =  new Venta();
public importedeuda = 0;
public cont = 0;


// CIERRA PAGINADOR
public idcliente = null;
public idsucursal = null;

  constructor(public entregaservice: EntregaService,
              public ventaservice: VentaService,
              public turnoservice: TurnoService,
              public reciboservice: ReciboService,
              public clienteservice: ClienteService,
              public route: ActivatedRoute,
              public location: Location) { }

  ngOnInit() {


    $(document).ready(function () {

      $('.collapsible').collapsible();
      $('.modal').modal();
    });

    this.idcliente = this.route.snapshot.paramMap.get('idcliente');
    this.idsucursal = this.route.snapshot.paramMap.get('sucursal');

    this.getAllTurnosSinEntrega();
    this.getAllFacturasSinPagar();
    this.getAllEntregasSinMonetizar();
    this.getAllRecibosByClienteConSaldo();
  }

  getAllTurnosSinEntrega(){

    this.turnoservice.findAllSinEntrega(this.idcliente, this.idsucursal).subscribe(data =>
      {
        this.turnos$ = data;
        this.sizeturnos = this.turnos$.length;
      },
        err => {
        console.log(err.error);
      }
     );
  }

  getAllEntregasSinMonetizar(){

    this.entregaservice.findAllSinMonetizar(this.idcliente).subscribe(data => {

      this.entregas$ = data;
      this.sizeentregas = this.entregas$.length;
    },
      err => {
        console.log(err.error);
      }
    );

  }

  getAllFacturasSinPagar(){

    const active = true;

    this.ventaservice.findAllByPage('cliente.id==' + this.idcliente + ';importerestante > 0',
     this.page, this.size, this.order, this.asc, active).subscribe(data => {

      
      this.ventas$ = data.content;
      this.isFirst = data.first;
      this.isLast = data.last;
      this.totalPages = data.totalPages;
      this.numItems = data.numberOfElements;
      this.sizeventas = this.ventas$.length;
    },
      err => {
        console.log(err.error);
      }
    );

  }

  generarEntregaTurno(t: Turno) {

    let flag = false;
    flag = confirm('Esta seguro que desea generar la entrega del Turno #' + t.id.id + '?');
    if (flag) {
      window.location.href = '/gestion/entregas/turnos/' + t.id.id + '/' + t.id.sucursal.id;
    }
  }

  anularVenta(id: number, sucursal: number) {

    let flag = false;
    flag = confirm('Esta seguro que desea eliminar la venta #?' + id);
    if (flag) {
    this.ventaservice.anularVenta(id, sucursal).subscribe(response =>
      window.location.reload(),
      
      error => M.toast({ html: 'Error: ' + error, class: 'red'}));
    }
  }

  anularEntrega(id: number, sucursal: number) {

    let flag = false;
    flag = confirm('Esta seguro que desea eliminar la entrega #?' + id);
    if (flag) {
    this.entregaservice.anularEntrega(id, sucursal).subscribe(response =>
      window.location.reload(),
      error => M.toast({ html: 'Error: ' + error, class: 'red'}));
    }
  }

  
  getAllRecibosByClienteConSaldo() {

    this.reciboservice.findAllByPage('cliente.id==' + this.idcliente + ';importerestante>0', 0, 1000,
     this.order, this.asc).subscribe(data => {

      this.recibosconsaldo$ = data.content;

      if(this.recibosconsaldo$.length > 0) {
        this.cantrecibos = true;
      }
      
      this.idcliente = null;
    },
      err => {
        M.toast({ html: 'Error: ' + err, class: 'red'});
      }
    );
  }

  asignarVenta(venta: Venta){

    this.ventaseleccionada = venta;

  }

  getImporteDeuda(){

    if(this.cont == 0){
    this.idcliente = this.route.snapshot.paramMap.get('idcliente');

    this.clienteservice.calcularDeuda(this.idcliente).subscribe(importedeuda => {
      this.importedeuda = importedeuda;
      this.cont++;
    });
  }
  }
}

