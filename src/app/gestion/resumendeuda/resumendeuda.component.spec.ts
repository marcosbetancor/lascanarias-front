import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumendeudaComponent } from './resumendeuda.component';

describe('ResumendeudaComponent', () => {
  let component: ResumendeudaComponent;
  let fixture: ComponentFixture<ResumendeudaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumendeudaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumendeudaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
