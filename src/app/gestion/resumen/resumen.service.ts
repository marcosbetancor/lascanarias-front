import { Injectable } from '@angular/core';
import { GenericCrudService } from '../genericcrud/generic-crud.service';
import { ReciboVenta } from '../reciboventa/reciboventa';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ResumenService extends GenericCrudService<ReciboVenta, number> {

  constructor(http: HttpClient) {
    super(environment.baseUrl + '/secured/recibosventas', http);
 }

 
}
