import { Component, OnInit } from '@angular/core';
import { ReciboventaService } from '../reciboventa/reciboventa.service';
import { ReciboVenta } from '../reciboventa/reciboventa';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Recibo } from '../recibo/recibo';
import { ReciboService } from '../recibo/recibo.service';
import { Sucursal } from '../users/sucursal';
declare var M: any;

@Component({
  selector: 'app-resumen',
  templateUrl: './resumen.component.html',
  styleUrls: ['./resumen.component.css'],
})
export class ResumenComponent implements OnInit {

  //PAGINADOR
  public page = 0; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente
  public totalPages: Number; //Número total de páginas
  public numItems: Number; //Total de items existentes
  public size = 5;
  public order = 'id.id';
  public asc = false;
  public isFirst = false;
  public isLast = false;
  public search = '';
  public searchrecibo = '';

  public searchreciboventas = '';

  // CIERRA PAGINADOR

  public recibosventas$: ReciboVenta[] = [];
  public idcliente;
  public recibos: Recibo[];
  public rv = new ReciboVenta();
  public rvbis = new ReciboVenta();
  public sucursales$: Sucursal[] = [];
  public sucursalselect = new Sucursal();
  public id: number;
  public listaidrecibos: string;
  public listaidrecibosunicos: number[] = [];
  public recibo = new Recibo();

  constructor(public reciboventaservice: ReciboventaService,
    public reciboservice: ReciboService,
    public route: ActivatedRoute,
    public location: Location,
    public router: Router) { }

  ngOnInit() {


    this.idcliente = this.route.snapshot.paramMap.get('idcliente');

    if (this.idcliente) {

      this.searchrecibo += 'cliente.id==' + this.idcliente;
      this.search += 'recibo.cliente.id==' + this.idcliente;
      this.page = 0;
    }
    this.getAllRecibos();
  }

  getAllRecibos() {

    this.reciboservice.findAllByPage(this.searchrecibo, this.page, this.size, this.order, this.asc)
      .subscribe(data => {
        this.recibos = data.content;
        this.isFirst = data.first;
        this.isLast = data.last;
        this.totalPages = data.totalPages;
        this.numItems = data.numberOfElements;
        this.getAllRecibosVentas();
      }
      );
  }

  getAllRecibosVentas() {

    let cont = 0;
    this.listaidrecibos = '(';

    if (this.recibos.length > 0) {
      this.recibos.forEach(x => {

        if (!this.listaidrecibosunicos.includes(x.id.id)) {
          this.listaidrecibos += x.id.id + ',';
          this.listaidrecibosunicos.push(x.id.id);
          cont++;
        }

      });

      if (cont > 0) {
        this.listaidrecibos += '0)';

        this.searchreciboventas = 'recibo.cliente.id==' + this.idcliente + ';';

        this.searchreciboventas += 'recibo.id.id=in=' + this.listaidrecibos;

        this.reciboventaservice.findAllByPage(this.searchreciboventas, this.page, this.size, this.order, this.asc)
          .subscribe(recibosventas => this.recibosventas$ = this.recibosventas$.concat(recibosventas.content));
      }
    }
  }

  desaplicar(rv: ReciboVenta) {

    this.rvbis = new ReciboVenta();
    this.rvbis.id = rv.id;

    let flag = false;
    flag = confirm('¿Esta seguro que desea desaplicar Venta #' + rv.venta.id.id + '?');
    if (flag) {
      this.reciboventaservice.desaplicar(this.rvbis).subscribe(data => {

        M.toast({ html: `Venta #${rv.venta.id.id} desaplicada`, classes: 'green' });

        window.location.reload();
      }, error => {  M.toast({ html: error, classes: 'red' }); }
      );
    }
  }

  goToPage(page: number) {

    this.page = page;

    this.getAllRecibos();
    this.getAllRecibosVentas();

  }

}

