import { TestBed } from '@angular/core/testing';

import { ResumenpalletService } from './resumenpallet.service';

describe('ResumenpalletService', () => {
  let service: ResumenpalletService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ResumenpalletService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
