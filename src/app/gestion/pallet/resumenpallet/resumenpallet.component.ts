import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ignoreElements } from 'rxjs-compat/operator/ignoreElements';
import { Recibo } from '../../recibo/recibo';
import { DeudaPallet } from '../shared/deudapallet';
import { Operacionpallets } from '../shared/operacionpallet';
import { ReciboPallet } from '../shared/recibopallet';
import { ResumenpalletService } from './resumenpallet.service';

@Component({
  selector: 'app-resumenpallet',
  templateUrl: './resumenpallet.component.html',
  styleUrls: ['./resumenpallet.component.css']
})
export class ResumenpalletComponent implements OnInit {

public idcliente: number;
public operaciones: Operacionpallets[];
public totalDeuda = 0;
public totalRecibo = 0;

 //PAGINADOR
 public page = 0; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente
 public totalPages: Number; //Número total de páginas
 public numItems: Number; //Total de items existentes
 public size = 50;
 public order = '';
 public asc = false;
 public isFirst = false;
 public isLast = false;
 public search = '';

  constructor(public resumenpalletservice: ResumenpalletService,
              public route: ActivatedRoute) { }

  ngOnInit(): void {

    // tslint:disable-next-line:radix
    this.idcliente = parseInt(this.route.snapshot.paramMap.get('idcliente'));
    this.search = 'cliente.id==' + this.idcliente;
    this.getAllOperaciones();

  }

  getAllOperaciones(){

    this.resumenpalletservice.findAllByPage(this.search, this.page, this.size, this.order, this.asc).subscribe(data => {

      this.operaciones =  data.content as Operacionpallets[];
      this.isFirst = data.first;
      this.isLast = data.last;
      this.totalPages = data.totalPages;
      this.numItems = data.numberOfElements;

      if(this.page == 0){
        this.sumaTotales(this.operaciones);
      }
    },
      err => {
        console.log(err.error);
      }
    );

  }

  sumaTotales(operaciones: Operacionpallets[]){

    this.totalDeuda = 0;
    this.totalRecibo = 0;

    operaciones.forEach(x =>{

      if(x.cantidadrestante > 0) {
        if((x as DeudaPallet).venta !== undefined) {
            this.totalDeuda += x.cantidadrestante;
        } else {
            this.totalRecibo += x.cantidadrestante;
        }
    }

    });
  }

 
  goToPage(page: number) {

    this.page = page;

    this.getAllOperaciones();

  }

  isInstanceOfDeuda (o: DeudaPallet | ReciboPallet) {

    if ((o as DeudaPallet).venta !== undefined) {

      return true;
    } else {
      return false;
    }
  }
}
