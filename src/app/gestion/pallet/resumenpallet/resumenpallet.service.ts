import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GenericCrudService } from '../../genericcrud/generic-crud.service';
import { Operacionpallets } from '../shared/operacionpallet';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';



const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' ,
  'Access-Control-Allow-Methods': 'POST,GET,PUT,PATCH,DELETE,OPTIONS',
  'Access-Control-Allow-Credentials': 'true'})
};

@Injectable({
  providedIn: 'root'
})
export class ResumenpalletService extends GenericCrudService<Operacionpallets, number>{

  constructor(http: HttpClient) {

    super(environment.baseUrl + '/secured/pallets', http);

   }
}
