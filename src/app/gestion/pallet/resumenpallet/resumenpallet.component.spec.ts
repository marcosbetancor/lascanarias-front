import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumenpalletComponent } from './resumenpallet.component';

describe('ResumenpalletComponent', () => {
  let component: ResumenpalletComponent;
  let fixture: ComponentFixture<ResumenpalletComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResumenpalletComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResumenpalletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
