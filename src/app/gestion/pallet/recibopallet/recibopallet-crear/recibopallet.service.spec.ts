import { TestBed } from '@angular/core/testing';

import { RecibopalletService } from './recibopallet.service';

describe('RecibopalletCrearService', () => {
  let service: RecibopalletService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RecibopalletService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
