import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GenericCrudService } from './../../../genericcrud/generic-crud.service';
import { ReciboPallet } from 'src/app/gestion/pallet/shared/recibopallet';

@Injectable({
  providedIn: 'root'
})
export class RecibopalletService extends GenericCrudService<ReciboPallet, number> {

  constructor(http: HttpClient) {
 
   super(environment.baseUrl + '/secured/pallets/recibo', http);
  }
}