import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/gestion/cliente/cliente';
import { ClienteService } from 'src/app/gestion/cliente/cliente.service';
import { RecibopalletService } from './recibopallet.service';
import { ReciboPallet } from 'src/app/gestion/pallet/shared/recibopallet';
declare var $: any;
declare var M: any;

@Component({
  selector: 'app-recibopallet-crear',
  templateUrl: './recibopallet-crear.component.html',
  styleUrls: ['./recibopallet-crear.component.css']
})
export class RecibopalletCrearComponent implements OnInit {

  clientes$: Cliente[];
  clienteselect: Cliente;
  recibopallet = new ReciboPallet();
  
  constructor(public clienteservice: ClienteService,
              public recibopalletservice: RecibopalletService) { }

  ngOnInit(): void {
  
    this.clienteservice.findAllSinPage().subscribe(clientes => this.clientes$ = clientes);
  }

  
  getAllClientes() {
    
    this.clienteservice.findAllSinPage().subscribe(clientes => this.clientes$ = clientes);
  }

  guardarRecibo(): void {

    $('.btncrear').hide();
      this.recibopalletservice.save(this.recibopallet).subscribe(
        
        response => M.toast({ html: 'Recibo pallet creado correctamente', class: 'green'}),
        error => M.toast({ html: 'No se pudo crear el recibo: ' + error, class: 'red'}),
        () => this.goBack());
        
    $('.btncrear').show();
      }

  goBack(): void {
    window.location.reload();
  }
}
