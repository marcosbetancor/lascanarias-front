import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecibopalletCrearComponent } from './recibopallet-crear.component';

describe('RecibopalletCrearComponent', () => {
  let component: RecibopalletCrearComponent;
  let fixture: ComponentFixture<RecibopalletCrearComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecibopalletCrearComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecibopalletCrearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
