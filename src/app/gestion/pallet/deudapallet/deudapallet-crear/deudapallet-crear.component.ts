import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/gestion/cliente/cliente';
import { ClienteService } from 'src/app/gestion/cliente/cliente.service';
import { DeudapalletService } from './deudapallet.service';
import { DeudaPallet } from 'src/app/gestion/pallet/shared/deudapallet';
declare var $: any;
declare var M: any;

@Component({
  selector: 'app-deudapallet-crear',
  templateUrl: './deudapallet-crear.component.html',
  styleUrls: ['./deudapallet-crear.component.css']
})
export class DeudapalletCrearComponent implements OnInit {

  public deudapallet = new DeudaPallet();
  public clientes$: Cliente[];
  public clienteselect: Cliente;
  constructor(public clienteservice: ClienteService,
              public deudaservice: DeudapalletService) { }

  ngOnInit(): void {

    this.getAllClientes();
  }

  
  getAllClientes() {
    
    this.clienteservice.findAllSinPage().subscribe(clientes => this.clientes$ = clientes);
  }

  guardarDeuda(): void {

    $('.btncrear').hide();
      this.deudaservice.save(this.deudapallet).subscribe(
        response => M.toast({ html: 'Deuda pallet creada correctamente' , class: 'green'}),
        error => M.toast({ html: 'No se pudo crear la deuda: ' + error, class: 'red'}),
        () => this.goBack());
        
    $('.btncrear').show();
      }

  goBack(): void {
    window.location.reload();
  }
}


