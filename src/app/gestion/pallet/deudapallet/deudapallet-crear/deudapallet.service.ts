import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GenericCrudService } from 'src/app/gestion/genericcrud/generic-crud.service';
import { environment } from 'src/environments/environment';
import { DeudaPallet } from 'src/app/gestion/pallet/shared/deudapallet';

@Injectable({
  providedIn: 'root'
})
export class DeudapalletService extends GenericCrudService<DeudaPallet, number> {

  constructor(http: HttpClient) {
 
   super(environment.baseUrl + '/secured/pallets/deudapallet', http);
 }
}
