import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeudapalletCrearComponent } from './deudapallet-crear.component';

describe('DeudapalletCrearComponent', () => {
  let component: DeudapalletCrearComponent;
  let fixture: ComponentFixture<DeudapalletCrearComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeudapalletCrearComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeudapalletCrearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
