import { TestBed } from '@angular/core/testing';

import { DeudapalletService } from './deudapallet.service';

describe('DeudapalletService', () => {
  let service: DeudapalletService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DeudapalletService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
