import { Cliente } from '../../cliente/cliente';

export class Operacionpallets{
    id: number;
    cliente: Cliente;
    cantidad: number;
    cantidadrestante: number;
    descripadic: string;
    created_at: Date;
    active: boolean;
}