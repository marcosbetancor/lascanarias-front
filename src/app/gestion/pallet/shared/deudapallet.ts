
import { Venta } from 'src/app/gestion/venta/venta';
import { Entrega } from '../../entrega/entrega';
import { Operacionpallets } from './operacionpallet';

export class DeudaPallet extends Operacionpallets{
    
    venta: Venta;
    entrega: Entrega;
}