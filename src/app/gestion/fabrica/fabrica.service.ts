import { Injectable } from '@angular/core';
import { Fabrica } from './fabrica';
import { GenericCrudService } from '../genericcrud/generic-crud.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FabricaService extends GenericCrudService<Fabrica, number> {

  constructor(http: HttpClient) {

    super(environment.baseUrl + '/secured/fabricas', http);
  }
}
