import { Producto } from '../producto/producto';
import { TipoProducto } from '../producto/tipoproducto/tipoproducto';

export class Fabrica {

    id: number;
    descrip: string;
    productos: Producto[];
    tipoproducto: TipoProducto;
}