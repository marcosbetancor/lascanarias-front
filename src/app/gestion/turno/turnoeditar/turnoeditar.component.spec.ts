import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TurnoeditarComponent } from './turnoeditar.component';

describe('TurnoeditarComponent', () => {
  let component: TurnoeditarComponent;
  let fixture: ComponentFixture<TurnoeditarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TurnoeditarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TurnoeditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
