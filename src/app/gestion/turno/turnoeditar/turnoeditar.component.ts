import { Component, OnInit } from '@angular/core';
import { FabricaService } from '../../fabrica/fabrica.service';
import { Fabrica } from '../../fabrica/fabrica';
import { Turno } from '../turno';
import { TurnoProducto } from '../turnoproducto';
import { TurnoService } from '../turno.service';
import { Location } from '@angular/common';
import { ChoferService } from '../chofer.service';
import { Chofer } from '../chofer';
import { ActivatedRoute, Router } from '@angular/router';
import { Cliente } from '../../cliente/cliente';
import { ClienteService } from '../../cliente/cliente.service';
import { DestinoService } from '../destino.service';
import { Destino } from '../destino';
declare var $: any;
declare  var M: any;

@Component({
  selector: 'app-turnoeditar',
  templateUrl: './turnoeditar.component.html',
  styleUrls: ['./turnoeditar.component.css']
})
export class TurnoeditarComponent implements OnInit {

  public fabricas$: Fabrica[] = [];
  public turno = new Turno();
  public turnoproducto = new TurnoProducto();
  public chofer = new Chofer();
  public choferes$: Chofer[] = [];
  public turnos: Turno[] = [];
  public destinos: Destino[] = [];
  public destino = new Destino();


  constructor(public fabricaservice: FabricaService,
    public turnoservice: TurnoService,
    public choferservice: ChoferService,
    public clienteservice: ClienteService,
    public destinoservice: DestinoService,
    public location: Location,
    public route: ActivatedRoute,
    public router: Router) { }

  ngOnInit() {

    
    this.getTurnoById();
    this.getAllChoferes();
    this.getAllDestinos();
    
    this.getAllFabricas();

    $(document).ready(function () {
      $('.modal').modal();
    });


  }

  getAllDestinos(){

    this.destinoservice.findAll().subscribe(destinos => {
      this.destinos = destinos.filter(x => x.active == true);
    
    },
      error => console.log(error));

  }


  getTurnoById(): void {

    const id = +this.route.snapshot.paramMap.get('id');
    const sucursal = +this.route.snapshot.paramMap.get('sucursal');
    if (id) {
      document.getElementById('crear').style.display = 'none';
      document.getElementById('modificar').style.display = 'block';
      this.turnoservice.findOneByIdAndSucursal(id, sucursal).subscribe(turno => this.turno = turno,
        error => console.log(error),
        () => this.CambiosVistaUpdate());
    }
  }

  CambiosVistaUpdate() {

    this.chofer = this.turno.chofer;
    this.hacerNombreApellidoCompleto();
  }

  getAllChoferes() {

    this.choferservice.findAll().subscribe(choferes => this.choferes$ = choferes,
      error => console.log('ERROR: ' + error),
      () => this.hacerNombreApellidoCompleto());
  }

  getAllFabricas() {
    this.fabricaservice.findAll().subscribe(fabricas => this.fabricas$ = fabricas);

    //this.fabricas$ = this.fabricas$.filter(x => x.tipoproducto.id = this.turno.tipoproducto.id);
  }

  eliminarFila(i: number) {
    this.turno.turnoproductos.splice(i, 1);
  }

  agregarLinea() {

    this.turno.turnoproductos.push(this.turnoproducto);
    this.turnoproducto = new TurnoProducto();
    $('#cant').focus();

    $(document).ready(function () {
      $(".dropdown-trigger").dropdown();
    });

  }

  goBack(): void {
    window.location.reload();
  }

  guardar(): void {

    if (this.turno.fabrica.descrip) {
      this.turnoservice.save(this.turno).subscribe(
        response => M.toast({ html: 'Turno creado', class: 'green'}),
        error => M.toast({ html: 'No se pudo crear el turno', class: 'red'}),
        () => this.goBack());
    } else {
      alert('Debe seleccionar fábrica');
    }
  }

  updateBySucursal() {

    
    const id = +this.route.snapshot.paramMap.get('id');
    const sucursal = +this.route.snapshot.paramMap.get('sucursal');
    this.turno.id = null;
    this.turnoservice.updateBySucursal(id, sucursal, this.turno).subscribe(data => {
      M.toast({ html: `<span>Turno modificado correctamente</span><a href='/gestion/turnos/${id}/${sucursal}' class='btn-flat toast-action'>Ver</a>`, classes: 'green'});

      }, error => { M.toast({ html: error, classes: 'red'}); }
    );

  }

  hacerNombreApellidoCompleto() {
    let i: Chofer;
    this.choferes$.forEach(function (i) {
      i.nombreapellido = i.nombre + ' ' + i.apellido;
    }
    );
  }
}
