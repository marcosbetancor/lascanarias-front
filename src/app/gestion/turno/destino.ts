import { Turno } from "./turno";

export class Destino{
    id: number;
    descrip: string;
    turnos: Turno[];
    active: boolean;
}