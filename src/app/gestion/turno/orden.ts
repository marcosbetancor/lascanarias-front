export class Orden{
    value: string;
    descrip: string;

    constructor(value?: string, descrip?: string){
        this.value = value;
        this.descrip = descrip;
    }
}
