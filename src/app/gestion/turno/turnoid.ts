import { Sucursal } from "../users/sucursal";

export class TurnoId{

    id: number;
    sucursal: Sucursal;
}