import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Turno } from './turno';
import { GenericCrudService } from '../genericcrud/generic-crud.service';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' ,
  'Access-Control-Allow-Methods': 'POST,GET,PUT,PATCH,DELETE,OPTIONS',
  'Access-Control-Allow-Credentials': 'true'})
};

@Injectable({
  providedIn: 'root'
})
export class TurnoService extends GenericCrudService<Turno, number> {

  constructor(http: HttpClient) {

    super(environment.baseUrl + '/secured/turnos', http);

   }

   findAllSinEntrega(cliente: number, sucursal: number): Observable<Turno[]> {
    return this.http.get<Turno[]>(this.base + '/' + cliente + '/sinentrega', httpOptions);
  }

  
}
