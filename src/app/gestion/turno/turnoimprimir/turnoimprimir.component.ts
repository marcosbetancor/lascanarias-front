import { Component, OnInit } from '@angular/core';
import { TurnoService } from '../turno.service';
import { Turno } from '../turno';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-turnoimprimir',
  templateUrl: './turnoimprimir.component.html',
  styleUrls: ['./turnoimprimir.component.css']
})
export class TurnoimprimirComponent implements OnInit {

  public turno = new Turno();

  constructor(public turnoservice: TurnoService,
    public route: ActivatedRoute,
    public location: Location) { }

  ngOnInit() {
    this.getTurnoById();
  }

  getTurnoById(): void {

    const id = +this.route.snapshot.paramMap.get('id');

    this.turnoservice.findOne(id).subscribe(turno => this.turno = turno,
      error => console.log(error),
      () => console.log('ok')
      );
    }

    print(): void{
      window.print();
    }
  }


