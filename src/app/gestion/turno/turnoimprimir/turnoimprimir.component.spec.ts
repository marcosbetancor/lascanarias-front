import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TurnoimprimirComponent } from './turnoimprimir.component';

describe('TurnoimprimirComponent', () => {
  let component: TurnoimprimirComponent;
  let fixture: ComponentFixture<TurnoimprimirComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TurnoimprimirComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TurnoimprimirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
