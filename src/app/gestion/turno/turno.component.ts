import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FabricaService } from '../fabrica/fabrica.service';
import { Fabrica } from '../fabrica/fabrica';
import { Turno } from './turno';
import { TurnoProducto } from './turnoproducto';
import { TurnoService } from './turno.service';
import { Location } from '@angular/common';
import { ChoferService } from './chofer.service';
import { Chofer } from './chofer';
import { ActivatedRoute, Router } from '@angular/router';
import { TipoproductoService } from '../producto/tipoproducto/tipoproducto.service';
import { TipoProducto } from '../producto/tipoproducto/tipoproducto';
import { UsersService } from '../users/users.service';
import { Sucursal } from '../users/sucursal';
import { SucursalService } from '../sucursal/sucursal.service';
import { Orden } from './orden';
import { Cliente } from '../cliente/cliente';
import { ClienteService } from '../cliente/cliente.service';
declare var $: any;
declare var M: any;

@Component({
  selector: 'app-turno',
  templateUrl: './turno.component.html',
  styleUrls: ['./turno.component.css']
})
export class TurnoComponent implements OnInit {

  public fabricas$: Fabrica[];
  public turno = new Turno();
  public turnoproducto = new TurnoProducto();
  public tipoproductos$: TipoProducto[];
  public chofer = new Chofer();
  public choferes$: Chofer[];
  public turnos: Turno[];
  public choferselect: Chofer[] = [];
  public tipoproductoselect: TipoProducto;

  public fabricaselect: Fabrica;
  public id: Number;
  public idasignado: Number;
  public sucursalselect: Sucursal;
  public sucursales$: Sucursal[];
  public clienteselect: Cliente;
  public clientes: Cliente[];
  public fechadesde: Date;
  public fechahasta: Date;
  public ordenamiento: Orden[] = [{'value': 'id.id', 'descrip': 'ID'}, {'value': 'fecharetiro', 'descrip': 'Fecha retiro'},{'value': 'created_at', 'descrip': 'Fecha creada'}];
  public ordenam = new Orden();
  public fabricasfilter: Fabrica[];
  public vistacompacta = false;
  

  //PAGINADOR
  public page = 0; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente
  public totalPages: Number; //Número total de páginas
  public numItems: Number; //Total de items existentes
  public size = 25;
  public order = 'created_at';
  public asc = false;
  public isFirst = false;
  public isLast = false;
  public search = '';

  // CIERRA PAGINADOR
  constructor(public fabricaservice: FabricaService,
    public turnoservice: TurnoService,
    public choferservice: ChoferService,
    public clienteservice: ClienteService,
    public sucursalservice: SucursalService,
    public tipoproductoservice: TipoproductoService,
    public usuarioservice: UsersService,
    public route: ActivatedRoute,
    public location: Location,
    public router: Router) { }

  @ViewChild('choferes') choferesRef: ElementRef;
  @ViewChild('tipoproductos') tipoProductosRef: ElementRef;
  @ViewChild('fabricas') fabricasRef: ElementRef;

  ngOnInit() {


    this.ordenam.value = 'id.id';

    this.getTurnoByIdAndSucursal();

    //Traemos en este hook los items de la primera página de nuestra colección
    this.getAllTurnosByPage();
    this.getAllFabricas();
    this.getAllChoferes();
    this.getAllTipoProductos();
    this.getAllClientes();
    this.getAllSucursales();


    $(document).ready(function () {
      $('.modal').modal();
    });

    $(document).ready(function () {
      $('.collapsible').collapsible();
    });

  }



  // PAGINADOR


  //Función para pasar de página

  //Esta función se ejecuta cada vez que se desencadena

  //un evento sobre el componente hijo (app-pagination)

  goToPage(page: number) {

    this.page = page;

    this.getAllTurnosByPage();

  }

  //Este método llama al servicio dónde se obtiene el listado de tiendas

  //Recibe una página concreta

  getAllTurnosByPage() {


    this.turnoservice.findAllByPage(this.search, this.page, this.size, this.ordenam.value, this.asc).subscribe(data => {

      this.turnos = data.content;
      this.isFirst = data.first;
      this.isLast = data.last;
      this.totalPages = data.totalPages;
      this.numItems = data.numberOfElements;
    },
      err => {
        console.log(err.error);
      }
    );

  }

  //CIERRA PAGINADOR
  searchFn() {

    this.search = '';

    if (this.idasignado != null) {
      this.search += 'idasignado==' + this.idasignado;
      this.page = 0;
      this.idasignado = null;
      this.getAllTurnosByPage();

    } else {

      if (this.id != null) {

        this.search += 'id.id==' + this.id;
        this.page = 0;
        this.id = null;
        this.getAllTurnosByPage();

      } else {

        if (this.sucursalselect != null) {

          this.search += 'id.sucursal.id==' + this.sucursalselect.id + ';';
        }
        if (this.choferselect != null) {

          if(this.choferselect.length > 1){
            
            this.choferselect.forEach(x =>{
              this.search += 'chofer.id==' + x.id + ',';
            });
            this.search = this.search.substring(0, this.search.length - 1);
          } else {
            
            this.search += 'chofer.id==' + this.choferselect[0].id + ';';
          }
        }


        if (this.clienteselect != null) {
          this.search += 'cliente.id.id==' + this.clienteselect.id + ';';
        }

        if (this.fabricaselect != null) {

          this.search += 'fabrica.id==' + this.fabricaselect.id + ';';

        }

        if (this.tipoproductoselect != null) {

          this.search += 'fabrica.tipoproducto.id==' + this.tipoproductoselect.id + ';';

        }

        if (this.fechadesde != null) {

          this.search += 'fechadesde==' + this.fechadesde + ';';

        }

        if (this.fechahasta != null) {

          this.search += 'fechahasta==' + this.fechahasta + ';';

        }

        this.choferselect = null;
        this.fabricaselect = null;
        this.tipoproductoselect = null;
        this.clienteselect = null;
        this.sucursalselect = null;
        this.fechadesde = null;
        this.fechahasta = null;
        this.id = null;
        this.search = this.remove(this.search);
        this.page = 0;
        this.getAllTurnosByPage();
      }
    }
  }

  getAllSucursales() {

    this.sucursalservice.findAll().subscribe(sucursales => this.sucursales$ = sucursales,
      error => console.log(error));
  }

  getAllClientes() {

    this.clienteservice.findAllSinPage().subscribe(clientes => {
      this.clientes = clientes.filter(x => x.active == true);
    
    },
      error => console.log(error));

  }

  getAllTipoProductos() {

    this.tipoproductoservice.findAll().subscribe(tipoproductos => this.tipoproductos$ = tipoproductos,
      error => console.log(error));
  }

  getTurnoByIdAndSucursal(): void {

    const id = +this.route.snapshot.paramMap.get('id');
    const sucursal = Number(this.route.snapshot.paramMap.get('sucursal'));
    if (id && sucursal) {
      document.getElementById('crear').style.display = 'none';
      document.getElementById('modificar').style.display = 'block';
      this.turnoservice.findOneByIdAndSucursal(id, sucursal).subscribe(turno => this.turno = turno,
        error => console.log(error),
        () => this.CambiosVistaUpdate());
    }
  }

  CambiosVistaUpdate() {

    this.chofer = this.turno.chofer;
    this.hacerNombreApellidoCompleto();
  }

  getAllChoferes() {

    this.choferservice.findAll().subscribe(choferes => this.choferes$ = choferes,
      error => console.log('ERROR: ' + error));
  }

  getAllFabricas() {
    this.fabricaservice.findAll().subscribe(fabricas => this.fabricas$ = fabricas);

  }

  eliminarFila(i: number) {
    this.turno.turnoproductos.splice(i, 1);
  }

  agregarLinea() {

    if(this.turnoproducto.cantidad != null && this.turnoproducto.cantidad > 0){

      if(this.turnoproducto.producto != null){
      this.turno.turnoproductos.push(this.turnoproducto);
      this.turnoproducto = new TurnoProducto();
      $('#cant').focus();

      $(document).ready(function () {
        $(".dropdown-trigger").dropdown();
      });

      $(document).ready(function(){
        $('.datepicker').datepicker();
      });
    } else {
      M.toast({ html: 'Debe seleccionar un producto', class: 'red'});
    }
    } else {
      
      M.toast({ html: 'La cantidad no es válida', class: 'red'});
    }
  }



  goBack(): void {
    window.location.reload();
  }

  guardar(): void {

    if (this.turno.fabrica.descrip) {
      this.turnoservice.save(this.turno).subscribe(data =>{

        
      M.toast({ html: 'Turno creado', class: 'green'});
        window.location.reload();

      },
        error => M.toast({ html: 'No se pudo crear el turno', class: 'red'}),
        () => M.toast({ html: 'Turno creado', class: 'green'}));
    } else {
      M.toast({ html: 'Debe seleccionar fábrica', class: 'red'});
    }
  }

  update() {

    const id = +this.route.snapshot.paramMap.get('id');
    this.turnoservice.update(id, this.turno).subscribe(data => {
      window.location.reload();
    }, error => { M.toast({ html: error, class: 'red'}); }
    );

}
  

  hacerNombreApellidoCompleto() {
    let i: Chofer;
    this.choferes$.forEach(function (i) {
      i.nombreapellido = i.nombre + ' ' + i.apellido;
    }
    );
  }

  remove(s: string) {

    const lastChar = s.charAt(s.length - 1);

    if (lastChar === ';') {
      s = s.slice(0, s.length - 1);
    } else {
      return s;
    }
    return s;
  }

  anular(id: number, sucursal: number) {

    let flag = false;
    flag = confirm('¿Esta seguro que desea eliminar el turno #' + id + '?');

    if(flag){
    this.turnoservice.anularTurno(id, sucursal).subscribe(response => {

      M.toast({ html: 'Turno anulado', class: 'red'});
      this.router.navigate(['/gestion/turnos']);
    }, error => { M.toast({ html: error.message , class: 'red'}); }
    );
  }
}

  cambiarFabricas(){

    this.fabricasfilter = this.fabricas$.filter(x => x.tipoproducto.id == this.turno.tipoproducto.id);
  }

  
  compactarVista(){
    
    if(this.vistacompacta){
     this.vistacompacta = false;
    } else{
      this.vistacompacta = true;
    }
    $('table').toggleClass('compacto');

  }
}
