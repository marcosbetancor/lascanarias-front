import { Component, OnInit } from '@angular/core';
import { Turno } from '../turno';
import { TurnoService } from '../turno.service';
import { ActivatedRoute } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-turnodetalle',
  templateUrl: './turnodetalle.component.html',
  styleUrls: ['./turnodetalle.component.css']
})
export class TurnodetalleComponent implements OnInit {

  public turno = new Turno();
  public materiales = '';

  constructor(
    public turnoservice: TurnoService,
    public route: ActivatedRoute) { }

  ngOnInit() {

    this.getTurnoById();

  }

  getTurnoById(): void {

    const id = +this.route.snapshot.paramMap.get('id');
    const sucursal = +this.route.snapshot.paramMap.get('sucursal');

    if (id) {
      this.turnoservice.findOneByIdAndSucursal(id, sucursal).subscribe(turno => {
        this.turno = turno;
      
      this.turno.turnoproductos.forEach(x => {
        
        this.materiales += x.cantidad + " " + x.producto.descrip + '%0A';
      });

      },
        error => console.log(error));
    }
  }


}


