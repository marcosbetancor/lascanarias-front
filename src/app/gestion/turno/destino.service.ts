import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GenericCrudService } from '../genericcrud/generic-crud.service';
import { Destino } from './destino';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' ,
  'Access-Control-Allow-Methods': 'POST,GET,PUT,PATCH,DELETE,OPTIONS',
  'Access-Control-Allow-Credentials': 'true'})
};

@Injectable({
  providedIn: 'root'
})
export class DestinoService extends GenericCrudService<Destino, number> {

  constructor(http: HttpClient) {

    super(environment.baseUrl + '/secured/destinos', http);

   }
}
