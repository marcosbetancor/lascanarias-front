import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TurnoporloteComponent } from './turnoporlote.component';

describe('TurnoporloteComponent', () => {
  let component: TurnoporloteComponent;
  let fixture: ComponentFixture<TurnoporloteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TurnoporloteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TurnoporloteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
