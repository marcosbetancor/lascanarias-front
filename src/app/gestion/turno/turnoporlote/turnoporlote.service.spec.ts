import { TestBed } from '@angular/core/testing';

import { TurnoporloteService } from './turnoporlote.service';

describe('TurnoporloteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TurnoporloteService = TestBed.get(TurnoporloteService);
    expect(service).toBeTruthy();
  });
});
