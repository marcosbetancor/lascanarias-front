import { Injectable } from '@angular/core';
import { GenericCrudService } from '../../genericcrud/generic-crud.service';
import { Turno } from '../turno';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' ,
  'Access-Control-Allow-Methods': 'POST,GET,PUT,PATCH,DELETE,OPTIONS',
  'Access-Control-Allow-Credentials': 'true'})
};

@Injectable({
  providedIn: 'root'
})
export class TurnoporloteService extends GenericCrudService<Turno, number> {

  constructor(http: HttpClient) {

    super(environment.baseUrl + '/secured/turnos/porlote', http);

   }

   upload(file: File): Observable<Turno[]> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    formData.append('reportProgress', 'true');
    return this.http.post<Turno[]>(environment.baseUrl + '/secured/turnos/porexcel', formData, httpOptions);
  }


}
