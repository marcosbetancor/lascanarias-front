import { Component, OnInit } from '@angular/core';
import { TurnoporloteService } from './turnoporlote.service';
import { Turno } from '../turno';
import { TurnoService } from '../turno.service';
import { ChoferService } from '../chofer.service';
import { FabricaService } from '../../fabrica/fabrica.service';
import { Chofer } from '../chofer';
import { Fabrica } from '../../fabrica/fabrica';
import { TurnoProducto } from '../turnoproducto';
import { TipoproductoService } from '../../producto/tipoproducto/tipoproducto.service';
import { TipoProducto } from '../../producto/tipoproducto/tipoproducto';
import { Cliente } from '../../cliente/cliente';
import { ClienteService } from '../../cliente/cliente.service';
import { Destino } from '../destino';
import { DestinoService } from '../destino.service';
import { Alert } from 'selenium-webdriver';
declare var M: any;
declare var $: any;

@Component({
  selector: 'app-turnoporlote',
  templateUrl: './turnoporlote.component.html',
  styleUrls: ['./turnoporlote.component.css']
})
export class TurnoporloteComponent implements OnInit {

  public turno: Turno = new Turno();
  public cant: number;
  public turnosguardados: Turno[] = [];
  public chofer = new Chofer();
  public choferes$: Chofer[];
  public fabricas$: Fabrica[];
  public destinoselect: Destino;
  public destinos: Destino[];
  public fabricasfilter: Fabrica[];
  public tipoproductos$: TipoProducto[];
  public file: File = null;

  constructor(public turnoporloteservice: TurnoporloteService,
    public turnoservice: TurnoService,
    public destinoservice: DestinoService,
    public fabricaservice: FabricaService,
    public choferservice: ChoferService,
    public tipoproductoservice: TipoproductoService) { }

  ngOnInit() {
    this.getAllTipoProductos();
    this.getAllFabricas();
    this.getAllChoferes();
    this.getAllDestinos();

    let today = new Date();
    today.setDate(today.getDate() + 1);
    this.turno.fecharetiro = today;

  }

  getAllTipoProductos() {

    this.tipoproductoservice.findAll().subscribe(tipoproductos => this.tipoproductos$ = tipoproductos,
      error => console.log(error));
  }
  getAllDestinos() {

    this.destinoservice.findAll().subscribe(destinos => this.destinos = destinos,
      error => console.log(error));

  }
  getAllChoferes() {

    this.choferservice.findAll().subscribe(choferes => this.choferes$ = choferes,
      error => console.log('ERROR: ' + error),
      () => this.hacerNombreApellidoCompleto());
  }

  getAllFabricas() {
    this.fabricaservice.findAll().subscribe(fabricas => this.fabricas$ = fabricas);
  }

  uploadExcel() {
    
    if(this.file){
    this.turnoporloteservice.upload(this.file).subscribe(
      response => M.toast({ html: response.length + ' turno(s) creados exitosamente', class: 'green' }),
    error => M.toast({ html: JSON.stringify(error), class: 'red' }));
  } else {
    M.toast({ html: 'No existe archivo a subir', class: 'red' });
  }
  }

  savePorLote() {

    if (this.turno.chofer.id == null) {
      this.turno.chofer = null;
    }


    this.turnoporloteservice.savePorLote(this.turno, this.cant).subscribe(
      response => M.toast({ html: this.cant + ' turno(s) creados exitosamente', class: 'green' }),
      error => M.toast({ html: JSON.stringify(error.error), class: 'red' }),
     );
    }

  hacerNombreApellidoCompleto() {
    let i: Chofer;
    this.choferes$.forEach(function (i) {
      i.nombreapellido = i.nombre + ' ' + i.apellido;
    }
    );
  }

  cambiarFabricas() {

    this.fabricasfilter = this.fabricas$.filter(x => x.tipoproducto.id == this.turno.tipoproducto.id);
  }

  handleFileInput(files: FileList): void {
    this.file = files.item(0);
}
}
