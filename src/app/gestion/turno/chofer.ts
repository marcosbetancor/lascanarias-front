import { Camion } from "../chofer/camion";

export class Chofer{

    id: Number;
    nombre: String;
    apellido: String;
    cuil: Number;
    dni: Number;
    nombreapellido: String;
    camiones: Camion[];
}