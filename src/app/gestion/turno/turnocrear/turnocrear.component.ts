import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Fabrica } from '../../fabrica/fabrica';
import { FabricaService } from '../../fabrica/fabrica.service';
import { ProductoService } from '../../producto/producto.service';
import { TipoProducto } from '../../producto/tipoproducto/tipoproducto';
import { TipoproductoService } from '../../producto/tipoproducto/tipoproducto.service';
import { SucursalService } from '../../sucursal/sucursal.service';
import { UsersService } from '../../users/users.service';
import { Chofer } from '../chofer';
import { ChoferService } from '../chofer.service';
import { Turno } from '../turno';
import { TurnoService } from '../turno.service';
import { TurnoProducto } from '../turnoproducto';

import { Location } from '@angular/common';
import { Cliente } from '../../cliente/cliente';
import { ClienteService } from '../../cliente/cliente.service';
import { Destino } from '../destino';
import { DestinoService } from '../destino.service';

declare var $: any;
declare var M: any;


@Component({
  selector: 'app-turnocrear',
  templateUrl: './turnocrear.component.html',
  styleUrls: ['./turnocrear.component.css']
})
export class TurnocrearComponent implements OnInit {


  public fabricas$: Fabrica[];
  public turno = new Turno();
  public turnoproducto = new TurnoProducto();
  public tipoproductos$: TipoProducto[];
  public chofer = new Chofer();
  public choferes$: Chofer[];
  public turnos: Turno[];
  public choferselect: Chofer;
  public tipoproductoselect: TipoProducto;

  public fabricaselect: Fabrica;
  public id: Number;
  public idasignado: Number;
  public destinoselect: Destino;
  public destinos: Destino[];
  public fabricasfilter: Fabrica[];
  public tipotransporte$ = [{id: 1, descrip: 'FOB'}, {id: 2, descrip: 'CIF'}];



  constructor(public fabricaservice: FabricaService,
    public turnoservice: TurnoService,
    public choferservice: ChoferService,
    public destinoservice: DestinoService,
    public productoservice: ProductoService,
    public tipoproductoservice: TipoproductoService,
    public usuarioservice: UsersService,
    public route: ActivatedRoute,
    public location: Location,
    public router: Router) { }

  ngOnInit(): void {

    
    this.getAllFabricas();
    this.getAllChoferes();
    this.getAllTipoProductos();
    this.getAllDestinos();

  }

  
  getAllDestinos() {

    this.destinoservice.findAll().subscribe(destinos => {
      this.destinos = destinos.filter(x => x.active == true);
    
    },
      error => console.log(error));

  }

  getAllTipoProductos() {

    this.tipoproductoservice.findAll().subscribe(tipoproductos => this.tipoproductos$ = tipoproductos,
      error => console.log(error));
  }

  
  getAllChoferes() {

    this.choferservice.findAll().subscribe(choferes => this.choferes$ = choferes,
      error => console.log('ERROR: ' + error),
      () => this.hacerNombreApellidoCompleto());
  }

  getAllFabricas() {
    this.fabricaservice.findAll().subscribe(fabricas => this.fabricas$ = fabricas);

  }

  
  eliminarFila(i: number) {
    this.turno.turnoproductos.splice(i, 1);
  }

  agregarLinea() {

    if(this.turnoproducto.cantidad != null && this.turnoproducto.cantidad > 0){

      if(this.turnoproducto.producto != null){
      this.turno.turnoproductos.push(this.turnoproducto);
      this.turnoproducto = new TurnoProducto();
      $('#cant').focus();

      $(document).ready(function () {
        $(".dropdown-trigger").dropdown();
      });

      $(document).ready(function(){
        $('.datepicker').datepicker();
      });
    } else {
      M.toast({ html: 'Debe seleccionar un producto', class: 'red'});
    }
    } else {
      M.toast({ html: 'La cantidad no es válida', class: 'red'});
    }
  }



  goBack(): void {
    window.location.reload();
  }

  guardar(): void {

    if (this.turno.fabrica.descrip) {
      this.turnoservice.save(this.turno).subscribe(data =>{

        M.toast({ html: 'Turno creado', class: 'green'});
        window.location.reload();

      },
        error => M.toast({ html: 'No se pudo crear el turno', class: 'red'}),
        () => M.toast({ html: 'Turno creado', class: 'green'}));
    } else {
      M.toast({ html: 'Debe seleccionar fábrica', class: 'red'});
    }
  }

  

  hacerNombreApellidoCompleto() {
    let i: Chofer;
    this.choferes$.forEach(function (i) {
      i.nombreapellido = i.nombre + ' ' + i.apellido;
    }
    );
  }

  remove(s: string) {

    const lastChar = s.charAt(s.length - 1);

    if (lastChar === ';') {
      s = s.slice(0, s.length - 1);
    } else {
      return s;
    }
    return s;
  }

  
  cambiarFabricas(){

    this.fabricasfilter = this.fabricas$.filter(x => x.tipoproducto.id == this.turno.tipoproducto.id);
  }

}
