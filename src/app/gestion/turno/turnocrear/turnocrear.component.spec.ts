import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TurnocrearComponent } from './turnocrear.component';

describe('TurnocrearComponent', () => {
  let component: TurnocrearComponent;
  let fixture: ComponentFixture<TurnocrearComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TurnocrearComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TurnocrearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
