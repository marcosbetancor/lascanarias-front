import { Fabrica } from '../fabrica/fabrica';
import { TurnoProducto } from './turnoproducto';
import { Chofer } from './chofer';
import { TurnoId } from './turnoid';
import { Entrega } from '../entrega/entrega';
import { TipoProducto } from '../producto/tipoproducto/tipoproducto';
import { Cliente } from '../cliente/cliente';
import { Empresa } from './empresa';
import { TipoTransporte } from './tipotransporte';
import { Destino } from './destino';

export class Turno{

    id: TurnoId;
    idasignado: number;
    fabrica: Fabrica = new Fabrica();
    turnoproductos: TurnoProducto[] = [];
    chofer: Chofer = new Chofer();
    fecharetiro: Date;
    impreso: boolean;
    entregado: boolean;
    papelrecibido: boolean;
    destino: Destino = new Destino();
    entrega: Entrega;
    tipoproducto: TipoProducto;
    active: boolean;
    descripadic: string;
    empresa: Empresa;
    tipotransporte: TipoTransporte;
}