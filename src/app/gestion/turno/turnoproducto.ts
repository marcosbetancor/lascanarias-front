import { Turno } from "./turno";
import { Producto } from "../producto/producto";

export class TurnoProducto{
    turno: Turno;
    producto: Producto;
    cantidad: number;
}