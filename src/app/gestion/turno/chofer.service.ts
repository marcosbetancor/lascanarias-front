import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Chofer } from './chofer';
import { GenericCrudService } from '../genericcrud/generic-crud.service';
import { environment } from 'src/environments/environment';
import { Camion } from '../chofer/camion';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' ,
  'Access-Control-Allow-Methods': 'POST,GET,PUT,PATCH,DELETE,OPTIONS',
  'Access-Control-Allow-Credentials': 'true'})
};

@Injectable({
  providedIn: 'root'
})
export class ChoferService extends GenericCrudService<Chofer, number> {

  constructor(http: HttpClient) {

    super(environment.baseUrl + '/secured/choferes', http);

   }

   anularCamion(id: number){
    
    return this.http.delete<Camion>(this.base + '/camion/' + id, httpOptions);
   }

   updateChofer(chofer: Chofer): Observable<Chofer>{
    return this.http.put<Chofer>(this.base + '/' + chofer.id, chofer, httpOptions);
   }
}
