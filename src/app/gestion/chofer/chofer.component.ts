import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Chofer } from '../turno/chofer';
import { ChoferService } from '../turno/chofer.service';
declare var M: any;

@Component({
  selector: 'app-chofer',
  templateUrl: './chofer.component.html',
  styleUrls: ['./chofer.component.css']
})
export class ChoferComponent implements OnInit {

  public chofer = new Chofer();
  constructor(public choferservice: ChoferService,
    public router: Router) { }

  ngOnInit(): void {
  }

  crearChofer() {

    this.choferservice.save(this.chofer).subscribe(chofer => {


      M.toast({ html: 'Chofer ' + this.chofer.nombre + ' ' + this.chofer.apellido + ' creado', class: 'green' });
      this.chofer = new Chofer();
    },
      error => M.toast({ html:'No se pudo crear el chofer: ' + error, class: 'red'}));
  }

}
