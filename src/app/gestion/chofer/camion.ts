import { Chofer } from "../turno/chofer";

export class Camion{
    id: number;
    chofer: Chofer;
    patentechasis: String;
    patenteenganche: String;
}