import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChofereditarComponent } from './chofereditar.component';

describe('ChofereditarComponent', () => {
  let component: ChofereditarComponent;
  let fixture: ComponentFixture<ChofereditarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChofereditarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChofereditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
