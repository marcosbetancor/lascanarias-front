import { Component, OnInit } from '@angular/core';
import { data } from 'jquery';
import { Chofer } from '../../turno/chofer';
import { ChoferService } from '../../turno/chofer.service';
import { Camion } from '../camion';
declare var M: any;

@Component({
  selector: 'app-chofereditar',
  templateUrl: './chofereditar.component.html',
  styleUrls: ['./chofereditar.component.css']
})
export class ChofereditarComponent implements OnInit {

  public camion = new Camion();
  public chofer = new Chofer();
  public choferes$: Chofer[] = [];

  constructor(
    public choferservice: ChoferService) { }

  ngOnInit(): void {
    this.getAllChoferes();
  }

  getAllChoferes() {

    this.choferservice.findAll().subscribe(choferes => this.choferes$ = choferes,
      error => console.log('ERROR: ' + error),
      () => this.hacerNombreApellidoCompleto());
  }

  hacerNombreApellidoCompleto() {
    let i: Chofer;
    this.choferes$.forEach(function (i) {
      i.nombreapellido = i.nombre + ' ' + i.apellido;
    }
    );
  }

  anular(id: number){
    this.choferservice.anularCamion(id).subscribe(data => {
      M.toast({ html: 'Camion eliminado correctamente', classes: 'green'});
      const removeIndex = this.chofer.camiones.map(item => item.id).indexOf(id);
      this.chofer.camiones.splice(removeIndex, 1);
    
      },
      error => M.toast({ html: 'Error: ' + error, classes: 'red'})
    );
  }

  agregar(){

    if(this.camion.patentechasis != null || this.camion.patenteenganche){
      
      this.chofer.camiones.push(this.camion);

      this.choferservice.updateChofer(this.chofer).subscribe( chofer => {

      
        M.toast({ html: 'Camion agregado correctamente', classes: 'green'});
  
      },
      error => (M.toast({ html: 'ERROR ' + error, classes: 'red'})));
    
    } else {
      
      M.toast({ html: 'Debe completar los datos del camion', classes: 'red'});
    }
  }
    

}
