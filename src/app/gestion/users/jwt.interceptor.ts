import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JwtToken } from './jwt.token';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    
    private jwt: JwtToken;
    

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // add authorization header with jwt token if available
        this.jwt = JSON.parse(localStorage.getItem('currentUser'));

        if(this.jwt) {

        const headers = new HttpHeaders({
            'Authorization': 'Bearer ' + this.jwt.access_token
          });

          headers.append('Access-Control-Allow-Origin', '*');
          headers.append('Access-Control-Allow-Credentials', 'true');
          headers.append('Access-Control-Allow-Methods', 'POST,GET,PUT,PATCH,DELETE,OPTIONS');

            request = request.clone({headers});

        return next.handle(request);

        } else {
            return next.handle(request);
        }
    }
}
