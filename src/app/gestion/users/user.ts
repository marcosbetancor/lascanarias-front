import { Sucursal } from './sucursal';


export class User{

    username: string;
    password: string;
    nombre: string;
    apellido: string;
    nombreapellido: string;
    sucursal: Sucursal;

}