import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { JwtToken } from './jwt.token';
import { User } from './user';
import { environment } from 'src/environments/environment';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
};

@Injectable({
  providedIn: 'root'
})

export class UsersService {

  public url = environment.baseUrl + '/oauth/token';
  public base = environment.baseUrl + '/vendedores';
  public http: HttpClient;
  public jwt: JwtToken;

  public headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
  .set('Authorization', 'Basic ' + btoa('spring-security-oauth2-read-write-client' + ':'
  + 'spring-security-oauth2-read-write-client-password1234'));

  constructor(http: HttpClient) {

    this.http = http;
  }

    
  login(username: string, password: string) {
      
    const _httpParams = new HttpParams()
    .set('username', username)
    .set('password', password)
    .set('grant_type', 'password')
    .set('client_id', 'spring-security-oauth2-read-write-client');

    return this.http.post<any>(this.url, _httpParams, {headers: this.headers})
    .pipe(
        map((response: Response) => {

          //console.log(JSON.stringify(response));
          this.jwt = JSON.parse(JSON.stringify(response));
         // login successful if there's a jwt token in the response
         const token = JSON.stringify(this.jwt.access_token);
         if (token) {
             // store username and jwt token in local storage to keep user logged in between page refreshes
             localStorage.setItem('currentUser', JSON.stringify( this.jwt));

             // return true to indicate successful login
             return true;
         } else {
             // return false to indicate failed login
             return false;
         }
        }));
}

    getToken(): String {
      const currentUser = JSON.parse(localStorage.getItem('currentUser'));
      const token = currentUser && currentUser.token;
      return token ? token : '';
    }

    getCurrentUser(){
      const currentUser = JSON.parse(localStorage.getItem('currentUser'));
      return currentUser;
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        localStorage.removeItem('currentUser');
  }

  handleError(error: HttpErrorResponse) {
    // return an observable with a user-facing error message
    return throwError("Error message...");
  }



}
