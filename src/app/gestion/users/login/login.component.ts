import { Component, OnInit, Input } from '@angular/core';
import { User } from '../user';
import { UsersService } from '../users.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
declare var M: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  @Input() user = new User();
  loading = false;
  error = '';
  
  constructor(public router: Router,
    public usersService: UsersService,
    public location: Location) { }

  ngOnInit() {
    this.usersService.logout();
  }

  login(user: User) {

    this.usersService.login(user.username, user.password).subscribe(result => {
      if (result === true) {
          // login successful
          this.router.navigate(['']);
      } else {
          // login failed
          this.error = 'Username or password is incorrect';
          this.loading = false;
          M.toast({ html: this.error, class: 'red'});
      }
  }, error => {
    this.loading = false;
    this.error = ' Username or password is incorrect';
    M.toast({ html: this.error, class: 'red'});
  });
  }



  goBack(): void {
    window.location.reload();
  }


}
