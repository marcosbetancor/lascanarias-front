import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GenericCrudService } from '../genericcrud/generic-crud.service';
import { TipoVenta } from './tipoventa';
import { environment } from 'src/environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class TipoVentaService extends GenericCrudService<TipoVenta, number> {

 constructor(http: HttpClient) {

  super(environment.baseUrl + '/secured/tipoventas', http);
}
}
