import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ProductoService } from '../producto/producto.service';
import { Producto } from '../producto/producto';
import { TipoProducto } from '../producto/tipoproducto/tipoproducto';
import { TipoVentaService } from './tipoventa.service';
import { TipoVenta } from './tipoventa';
import { Venta } from './venta';
import { LineaVenta } from './lineaventa';
import { UsersService } from '../users/users.service';
import { VentaService } from './venta.service';
import { Location } from '@angular/common';
import { VendedorService } from './vendedor.service';
import { Vendedor } from './vendedor';
import { ClienteService } from '../cliente/cliente.service';
import { Cliente } from '../cliente/cliente';
import { ActivatedRoute, Router } from '@angular/router';
import { EntregaService } from '../entrega/entrega.service';
import { Entrega } from '../entrega/entrega';
import { DeudaPallet } from '../pallet/shared/deudapallet';
import { isNumeric } from 'rxjs/internal-compatibility';
declare var $: any;
declare var M: any;

@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls: ['./venta.component.css'],
})
export class VentaComponent implements OnInit {

  public date: Date;
  public productos$: Producto[];
  public producto = new Producto(new TipoProducto());
  public tiposventa$: TipoVenta[];
  public tipoventa: TipoVenta;
  public venta = new Venta();
  public lineaventa = new LineaVenta();
  public total = 0.0;
  public vendedores$: Vendedor[];
  public clientes$: Cliente[];
  public clienteselect: Cliente;
  public entrega: Entrega;
  loading = false;
  public cantidadpallets = 0;
  public deudapallet = new DeudaPallet();

  // PAGINADOR
  public page = 0; // Número de página en la que estamos. Será 1 la primera vez que se carga el componente
  public totalPages: Number; // Número total de páginas
  public numItems: Number; // Total de items existentes
  public size = 1000000;
  public order = 'id';
  public asc = false;
  public isFirst = false;
  public isLast = false;
  public search = '';

  // CIERRA PAGINADOR

  constructor(public productoservice: ProductoService,
    public entregaservice: EntregaService,
    public tipoventaservice: TipoVentaService,
    public usuarioservice: UsersService,
    public clienteservice: ClienteService,
    public ventaservice: VentaService,
    public vendedorservice: VendedorService,
    public location: Location,
    public route: ActivatedRoute,
    public router: Router) {

    this.date = new Date();

  }

  ngOnInit() {


    this.getEntrega();
    this.getAllProductos();
    this.getAllTiposVentas();
    this.getAllVendedores();
    if (this.entrega === null || this.entrega === undefined) {
      this.getAllClientes();
    }
  }

  getEntrega() {

    this.loading = true;
    const id = +this.route.snapshot.paramMap.get('id');
    const sucursal = +this.route.snapshot.paramMap.get('sucursal');

    if (id && sucursal) {

      this.entregaservice.findOneByIdAndSucursal(id, sucursal).subscribe(
        entrega => {
          this.entrega = entrega,
            this.venta.cliente = this.entrega.cliente;
          this.venta.direccion = this.entrega.direccion;
          this.venta.telefono = this.entrega.telefono;
          this.venta.vendedor = this.entrega.vendedor;
          this.entrega.lineasentregas.forEach(x => {

            this.lineaventa = new LineaVenta();
            this.lineaventa.cantidad = x.cantidad;
            this.lineaventa.producto = x.producto;
            this.lineaventa.descripadic = x.descripadic;
            this.lineaventa.precio = this.lineaventa.producto.precio;

            this.venta.lineasventas.push(this.lineaventa);
            this.lineaventa = new LineaVenta();


          });


          this.actualizaTotal();
        },
        () => this.loading = false);
    }
  }


  getAllClientes() {
    this.loading = true;
    this.clienteservice.findAllByPage(this.search, this.page, this.size, this.order, this.asc, true)
      .subscribe(clientes => this.clientes$ = clientes.content,
        () => this.loading = false);
  }

  getAllProductos() {

    this.loading = true;
    this.productoservice.findAll().subscribe(productos => this.productos$ = productos,
      () => this.loading = false);
  }

  getAllTiposVentas() {

    this.loading = true;
    this.tipoventaservice.findAll().subscribe(tiposventa => this.tiposventa$ = tiposventa,
      error => console.log('ERROR: ' + error),
      () => {
        this.initSelect();
        this.loading = false;
      });
  }

  getAllVendedores() {

    this.loading = true;
    this.vendedorservice.findAll().subscribe(vendedores => this.vendedores$ = vendedores,
      error => console.log('ERROR: ' + error),
      () => {
        this.hacerNombreApellidoCompleto();
        this.loading = false;
      });
  }

  agregarLinea() {

    if (this.lineaventa.precio != null && this.lineaventa.producto != null && this.lineaventa.cantidad) {
      this.total += this.lineaventa.precio * this.lineaventa.cantidad;
      this.venta.lineasventas.push(this.lineaventa);
      this.lineaventa = new LineaVenta();
      $('#cant').focus();

      $(document).ready(function () {
        $('.dropdown-trigger').dropdown();
      });
    } else {
      alert('La cantidad, el producto y el precio son obligatorios');
    }
  }

  actualizaPrecio() {
    this.lineaventa.precio = this.lineaventa.producto.precio;
  }

  actualizaTotal() {

    this.total = 0;

    this.venta.lineasventas.forEach(x => {

      this.total += x.cantidad * x.precio;
    });
  }

  hacerNombreApellidoCompleto() {
    let i: Vendedor;
    this.vendedores$.forEach(function (i) {
      i.nombreapellido = i.nombre + ' ' + i.apellido;
    }
    );
  }

  eliminarFila(i: number, subtotal: number) {

    this.total -= subtotal;
    this.venta.lineasventas.splice(i, 1);
  }


  goBack(): void {
    window.location.reload();
  }


  guardarVenta(): void {

    this.loading = true;

    $('#vender').hide();

    if (this.venta.cliente != null) {

      if (this.venta.vendedor != null) {
        const id = +this.route.snapshot.paramMap.get('id');
        const sucursal = +this.route.snapshot.paramMap.get('sucursal');
        this.venta.sucursal = this.venta.vendedor.sucursal;



        if (id && sucursal) {

          this.guardarEntregaVenta(this.venta);

        } else {
          this.ventaservice.save(this.venta).subscribe(data => {

            this.venta = data;

            M.toast({ html: 'Venta #' + this.venta.id.id + ' creada correctamente', classes: 'green' });

            this.router.navigate(['/gestion/ventas/' + this.venta.id.id + '/' + this.venta.id.sucursal.id]);

          }, error => {
            alert(error);
            $('#vender').show();
          },

            () => this.loading = false
          );
        }
      } else {

    $('#vender').show();
        M.toast({ html: 'Debe elegir vendedor', classes: 'red' });
   
      }
    } else {
      
    $('#vender').show();
      M.toast({ html: 'Debe elegir cliente', classes: 'red' });
    }


    $('#btnvender').show();
  }

  guardarEntregaVenta(venta: Venta): void {

    $('#btnvender').hide();
    if (this.venta.cliente != null) {

      if (this.venta.vendedor != null) {
        const id = +this.route.snapshot.paramMap.get('id');
        const sucursal = +this.route.snapshot.paramMap.get('sucursal');

        if (id && sucursal) {

          this.venta.entregas.push(this.entrega);

          this.entrega.turno = null;

          this.ventaservice.save(this.venta).subscribe(data => {

            this.venta = data;


            M.toast({ html: 'Venta #' + this.venta.id.id + ' creada correctamente', classes: 'green' });

            this.router.navigate(['/gestion/ventas/' + this.venta.id.id + '/' + this.venta.id.sucursal.id]);

          }, error => {
            M.toast({ html: error, classes: 'red' });
            $('#vender').show();
          }
          );
        }
      } else {
        M.toast({ html: 'Debe elegir vendedor', classes: 'red' });

        $('#vender').show();
      }
    } else {
      M.toast({ html: 'Debe elegir cliente', classes: 'red' });

      $('#vender').show();
    }

    $('#vender').show();
  }

  calcularPallets() {

    if (this.entrega == null) {

      let cant = 0;

      this.venta.lineasventas.forEach(x => {

        if (isNumeric(x.producto.cantporpallet)) {
          cant += Math.ceil(x.cantidad / x.producto.cantporpallet);
        }
      });

      this.venta.cantidadpallets = cant;

      if (this.venta.cantidadpallets > 0) {
        $("#modalpallet").modal('open');
      } else {
        this.guardarVenta();
      }
    } else {
      this.guardarVenta();
    }
  }

  initSelect() {
    $(document).ready(function () {
      $('.modal').modal();
      $('.dropdown-trigger').dropdown();
      (<any>$('select')).formSelect();
    });
  }

}
