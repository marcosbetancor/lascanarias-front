import { Sucursal } from "../users/sucursal";

export class VentaID{
    id: number;
    sucursal: Sucursal;
}