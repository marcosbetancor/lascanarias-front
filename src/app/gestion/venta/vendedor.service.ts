import { Injectable } from '@angular/core';
import { Vendedor } from './vendedor';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GenericCrudService } from '../genericcrud/generic-crud.service';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class VendedorService extends GenericCrudService<Vendedor, number> {

  constructor(http: HttpClient) {
    super(environment.baseUrl + '/vendedores', http);
 }
}
