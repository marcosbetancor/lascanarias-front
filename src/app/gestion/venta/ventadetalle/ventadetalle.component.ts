import { Component, OnInit } from '@angular/core';
import { VentadetalleService } from './ventadetalle.service';
import { ActivatedRoute } from '@angular/router';
import { Venta } from '../venta';

@Component({
  selector: 'app-ventadetalle',
  templateUrl: './ventadetalle.component.html',
  styleUrls: ['./ventadetalle.component.css']
})
export class VentadetalleComponent implements OnInit {

  public venta = new Venta();

  constructor(public ventadetalleservice: VentadetalleService,
              public route: ActivatedRoute) { }

  ngOnInit() {
    this.getVenta();
  }

  getVenta() {

    const id = +this.route.snapshot.paramMap.get('id');
    const sucursal = +this.route.snapshot.paramMap.get('sucursal');

    if (id && sucursal) {
      this.ventadetalleservice.findOneByIdAndSucursal(id, sucursal).subscribe(venta => {
        this.venta = venta,
        this.calcularTotalPorVenta();
      },
        error => console.log(error));
    }

  }

  calcularTotalPorVenta() {

    let total = 0;

    this.venta.lineasventas.forEach(lv => {
        total += lv.cantidad * lv.precio;
        
      });

      this.venta.total = Number(total.toFixed(2));
    }
  }


