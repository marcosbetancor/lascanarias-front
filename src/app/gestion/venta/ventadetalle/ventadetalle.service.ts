import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { GenericCrudService } from '../../genericcrud/generic-crud.service';
import { Venta } from '../venta';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class VentadetalleService extends GenericCrudService<Venta, number> {

  constructor(http: HttpClient) {
    super(environment.baseUrl + '/secured/ventas', http);
 }
}
