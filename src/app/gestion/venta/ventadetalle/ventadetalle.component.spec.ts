import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VentadetalleComponent } from './ventadetalle.component';

describe('VentadetalleComponent', () => {
  let component: VentadetalleComponent;
  let fixture: ComponentFixture<VentadetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VentadetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VentadetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
