import { TestBed } from '@angular/core/testing';

import { VentadetalleService } from './ventadetalle.service';

describe('VentadetalleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VentadetalleService = TestBed.get(VentadetalleService);
    expect(service).toBeTruthy();
  });
});
