import { Producto } from '../producto/producto';

export class LineaVenta {

    cantidad: number;
    producto: Producto;
    precio: number;
    descripadic = '';
}
