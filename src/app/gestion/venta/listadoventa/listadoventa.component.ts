import { Component, OnInit } from '@angular/core';
import { VentaService } from '../venta.service';
import { Venta } from '../venta';
import { SucursalService } from '../../sucursal/sucursal.service';
import { Sucursal } from '../../users/sucursal';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Cliente } from '../../cliente/cliente';
import { ClienteService } from '../../cliente/cliente.service';
import { Recibo } from '../../recibo/recibo';
import { ReciboService } from '../../recibo/recibo.service';
declare var $: any;
declare var M: any;

@Component({
  selector: 'app-listadoventa',
  templateUrl: './listadoventa.component.html',
  styleUrls: ['./listadoventa.component.css']
})
export class ListadoventaComponent implements OnInit {

  //PAGINADOR
  public page = 0; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente
  public totalPages: Number; //Número total de páginas
  public numItems: Number; //Total de items existentes
  public size = 25;
  public order = 'id.id';
  public asc = false;
  public isFirst = false;
  public isLast = false;
  public search = '';

  // CIERRA PAGINADOR

  public id: number;
  public ventas$: Venta[];
  public sucursales$: Sucursal[];
  public sucursalselect: Sucursal;
  public clientes$: Cliente[];
  public clienteselect: Cliente;
  public idcliente = null;
  public recibos$: Recibo[];
  public recibosconsaldo$: Recibo[];
  public cantrecibos: Boolean;
  public ventaseleccionada = new Venta();
  loading = false;

  constructor(public ventaservice: VentaService,
    public reciboservice: ReciboService,
    public sucursalservice: SucursalService,
    public clienteservice: ClienteService,
    public route: ActivatedRoute,
    public location: Location,
    public router: Router) { }

  ngOnInit() {


    $(document).ready(function () {

      $('.collapsible').collapsible();
      $('.modal').modal();

    });

    this.idcliente = this.route.snapshot.paramMap.get('idcliente');

    if (this.idcliente) {

      this.search = 'cliente.id==' + this.idcliente;
      this.page = 0;
      this.getAllVentasByPage();
      this.search = '';
    } else {
      this.getAllVentasByPage();
    }
    this.getAllSucursales();
    this.getAllClientes();
  }


  goToPage(page: number) {

    this.page = page;

    this.getAllVentasByPage();

  }

  getAllVentasByPage() {

    this.loading = true;
    this.ventaservice.findAllByPage(this.search, this.page, this.size, this.order, this.asc, false).subscribe(data => {

      this.ventas$ = data.content;
      this.isFirst = data.first;
      this.isLast = data.last;
      this.totalPages = data.totalPages;
      this.numItems = data.numberOfElements;
    },
      err => {
        console.log(err.error);
      },
      () => this.loading = false
    );

  }



  //CIERRA PAGINADOR

  getAllClientes() {
    this.clienteservice.findAllSinPage().subscribe(clientes => this.clientes$ = clientes,
      error => console.log(error));
  }

  getAllSucursales() {

    this.sucursalservice.findAll().subscribe(sucursales => this.sucursales$ = sucursales,
      error => console.log(error));
  }

  anular(id: number, sucursal: number) {


    let flag = false;
    flag = confirm('Esta seguro que desea eliminar la venta #?' + id);
    if (flag) {
      this.ventaservice.anularVenta(id, sucursal).subscribe(data => {

        
        M.toast({ html: 'Venta anulada', class: 'red'});
        window.location.reload();
      }, error => { M.toast({ html: error, class: 'red'}); }
      );
    }
  }

  goBack(): void {
    window.location.reload();
  }

  searchFn() {

    this.search = '';

    if (this.id != null && this.sucursalselect != null) {

      this.search += 'id.id==' + this.id + ';id.sucursal.id==' + this.sucursalselect.id;
      this.page = 0;
      this.id = null;
      this.sucursalselect = null;
      this.getAllVentasByPage();
    }

    if (this.id != null) {

      this.search += 'id.id==' + this.id;
      this.page = 0;
      this.id = null;
      this.getAllVentasByPage();

    }

    if (this.sucursalselect != null) {

      this.search += 'id.sucursal.id==' + this.sucursalselect.id;
      this.page = 0;
      this.id = null;
      this.getAllVentasByPage();
    }

    if (this.clienteselect != null) {

      this.search += 'cliente.id==' + this.clienteselect.id;
      this.page = 0;
      this.id = null;
      this.getAllVentasByPage();
    }

    this.getAllVentasByPage();

  }

  asignarVenta(venta: Venta) {

    this.ventaseleccionada = venta;

  }

}

