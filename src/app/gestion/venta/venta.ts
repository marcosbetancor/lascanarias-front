import { TipoVenta } from './tipoventa';
import { LineaVenta } from './lineaventa';
import { Sucursal } from '../users/sucursal';
import { Vendedor } from './vendedor';
import { Cliente } from '../cliente/cliente';
import { VentaID } from './ventaID';
import { Entrega } from '../entrega/entrega';

export class Venta {

    id: VentaID;
    vendedor: Vendedor;
    tipoventa = new TipoVenta();
    fecha = new Date();
    direccion: String;
    telefono: String;
    lineasventas: LineaVenta[] = [];
    sucursal: Sucursal;
    cliente: Cliente;
    created_at: Date;
    total: number;
    active: boolean;
    entregas: Entrega[] = [];
    importerestante: number;
    cantidadpallets = 0;
}
