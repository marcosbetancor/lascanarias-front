import { Sucursal } from "./sucursal";

export class Vendedor {
    id: number;
    nombre: string;
    apellido: string;
    nombreapellido: string;
    sucursal: Sucursal;
}