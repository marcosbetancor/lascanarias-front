
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { IGenericCrud } from './igeneric-crud';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' ,
  'Access-Control-Allow-Methods': 'POST,GET,PUT,PATCH,DELETE,OPTIONS',
  'Access-Control-Allow-Credentials': 'true'})
};


export abstract class GenericCrudService <T, ID> implements IGenericCrud<T, ID> {

  protected base: string;
  protected http: HttpClient;


  constructor(
    base: string,
    http: HttpClient
  ) {
    this.base = base;
    this.http = http;
}


  save(t: T) {
    console.log(t);
    return this.http.post<T>(this.base, t, httpOptions);
  }

  savePorLote(t: T, cant: number) {
    console.log(t);
    return this.http.post<T>(this.base + '/' + cant, t, httpOptions);
  }
  update(id: ID, t: T) {
    return this.http.put<T>(this.base + '/' + id, t, httpOptions);
  }

  updateBySucursal(id: ID, sucursal: number, t: T) {
    return this.http.put<T>(this.base + '/' + id + '/' + sucursal, t, httpOptions);
  }

  findOne(id: ID): Observable<T> {
    return this.http.get<T>(this.base + '/' + id, httpOptions);
  }

  findOneByIdAndSucursal(id: ID, sucursal: number): Observable<T> {
    return this.http.get<T>(this.base + '/' + id + '/' + sucursal, httpOptions);
  }
  findAll(): Observable<T[]> {
    return this.http.get<T[]>(this.base, httpOptions);
  }

  findAllByPage(search: string, page: number, size: number, order: string, asc: boolean, active?: boolean): Observable<any> {
    return this.http.get<any>(this.base + `?search=${search}&page=${page}&size=${size}&order=${order}&asc=${asc}&active=${active}`, httpOptions);
  }

  delete(id: ID) {
    return this.http.delete(this.base + '/' + id, httpOptions)
    .pipe(map(this.extractData),
      catchError(this.handleError));
  }

  deleteWithSucursal(id: ID, idsucursal: number) {
    return this.http.delete(this.base + '/' + id + '/' + idsucursal, httpOptions)
    .pipe(map(this.extractData),
      catchError(this.handleError));
  }

  anularVenta(id: ID, sucursal: number){
    return this.http.delete(this.base + '/' + id + '/' + sucursal, httpOptions)
    .pipe(map(this.extractData),
      catchError(this.handleError));
  }

  anularEntrega(id: ID, sucursal: number){
    return this.http.delete(this.base + '/' + id + '/' + sucursal, httpOptions)
    .pipe(map(this.extractData),
      catchError(this.handleError));
  }

  anularRecibo(id: ID, sucursal: number){
    return this.http.delete(this.base + '/' + id + '/' + sucursal, httpOptions)
    .pipe(map(this.extractData),
      catchError(this.handleError));
  }

  anularTurno(id: ID, sucursal: number){
    return this.http.delete(this.base + '/' + id + '/' + sucursal, httpOptions)
    .pipe(map(this.extractData),
      catchError(this.handleError));
  }

  protected extractData(res: Response) {
    const body = res || '';
    return body;
  }

  protected handleError(error: Response | any) {
    let msg: string;
    if(error instanceof Response) {
      msg = error.statusText || '';
    } else {
      msg = error.message ? error.message : error.toString();
    }

    return msg;
  }

}
