
import { BrowserModule} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ProductoModule } from './gestion/producto/producto.module';
import { UsersModule } from './gestion/users/users.module';
import { VentaComponent } from './gestion/venta/venta.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { VentadetalleComponent } from './gestion/venta/ventadetalle/ventadetalle.component';
import { ClienteComponent } from './gestion/cliente/cliente.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { TurnoComponent } from './gestion/turno/turno.component';
import { TurnoproductoComponent } from './gestion/turnoproducto/turnoproducto.component';
import { TurnoporloteComponent } from './gestion/turno/turnoporlote/turnoporlote.component';
import { TurnoimprimirComponent } from './gestion/turno/turnoimprimir/turnoimprimir.component';
import { PaginadorComponent } from './paginador/paginador.component';
import { TurnodetalleComponent } from './gestion/turno/turnodetalle/turnodetalle.component';
import { TurnoeditarComponent } from './gestion/turno/turnoeditar/turnoeditar.component';
import { AuthGuard } from './gestion/users/auth.guard';
import { GestionComponent } from './gestion/gestion.component';
import { ListadoventaComponent } from './gestion/venta/listadoventa/listadoventa.component';
import { EntregaComponent } from './gestion/entrega/entrega.component';
import { ListadoComponent } from './gestion/entrega/listado/listado.component';
import { DetalleComponent } from './gestion/entrega/detalle/detalle.component';
import { ListadoclienteComponent } from './gestion/cliente/listadocliente/listadocliente.component';
import { PrintComponent } from './print/print.component';
import { DetallereciboComponent } from './gestion/recibo/detallerecibo/detallerecibo.component';
import { ListadoreciboComponent } from './gestion/recibo/listadorecibo/listadorecibo.component';
import { ReciboComponent } from './gestion/recibo/recibo.component';
import { ReciboventaComponent } from './gestion/reciboventa/reciboventa.component';
import { ResumenComponent } from './gestion/resumen/resumen.component';
import { ResumendeudaComponent } from './gestion/resumendeuda/resumendeuda.component';
import { environment } from '../environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';
import { TurnocrearComponent } from './gestion/turno/turnocrear/turnocrear.component';
import { ChoferComponent } from './gestion/chofer/chofer.component';
import { DeudapalletCrearComponent } from './gestion/pallet/deudapallet/deudapallet-crear/deudapallet-crear.component';
import { RecibopalletCrearComponent } from './gestion/pallet/recibopallet/recibopallet-crear/recibopallet-crear.component';
import { ResumenpalletComponent } from './gestion/pallet/resumenpallet/resumenpallet.component';
import { ChofereditarComponent } from './gestion/chofer/chofereditar/chofereditar.component';

@NgModule({
  declarations: [
    AppComponent,
    VentaComponent,
    VentadetalleComponent,
    ClienteComponent,
    HomeComponent,
    HeaderComponent,
    TurnoComponent,
    TurnoproductoComponent,
    TurnoporloteComponent,
    TurnoimprimirComponent,
    PaginadorComponent,
    TurnodetalleComponent,
    TurnoeditarComponent,
    GestionComponent,
    ListadoventaComponent,
    EntregaComponent,
    ListadoComponent,
    DetalleComponent,
    ListadoclienteComponent,
    PrintComponent,
    ReciboComponent,
    DetallereciboComponent,
    ListadoreciboComponent,
    ReciboventaComponent,
    ResumenComponent,
    ResumendeudaComponent,
    TurnocrearComponent,
    ChoferComponent,
    DeudapalletCrearComponent,
    RecibopalletCrearComponent,
    ResumenpalletComponent,
    ChofereditarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ProductoModule,
    UsersModule,
    NgSelectModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  exports: [
    PrintComponent
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
