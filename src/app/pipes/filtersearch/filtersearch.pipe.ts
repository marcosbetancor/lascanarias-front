import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtersearch'
})
export class FiltersearchPipe implements PipeTransform {

  transform(value: any, args?: string): any {

    if(!value)return null;
    if(!args || args.length < 3) return value;

    args = args.toLowerCase();

    return value.filter(function(item) {
        return JSON.stringify(item).toLowerCase().includes(args);
    });
}

}
