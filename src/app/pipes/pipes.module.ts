import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiltersearchPipe } from './filtersearch/filtersearch.pipe';
import { GroupByPipe } from './group-by.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ FiltersearchPipe, GroupByPipe ],
  exports: [ FiltersearchPipe ]
})
export class PipesModule {

  static forRoot(): ModuleWithProviders<PipesModule> {
    return {
        ngModule: PipesModule,
        providers: [],
    };
}
}
