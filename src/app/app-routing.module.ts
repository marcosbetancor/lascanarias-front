import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductoComponent } from './gestion/producto/producto.component';
import { TipoproductoComponent } from './gestion/producto/tipoproducto/tipoproducto.component';
import { ProductoDetailComponent } from './gestion/producto/producto-detail/producto-detail.component';
import { LoginComponent } from './gestion/users/login/login.component';
import { VentaComponent } from './gestion/venta/venta.component';
import { HomeComponent } from './home/home.component';
import { TurnoComponent } from './gestion/turno/turno.component';
import { TurnoporloteComponent } from './gestion/turno/turnoporlote/turnoporlote.component';
import { TurnoimprimirComponent } from './gestion/turno/turnoimprimir/turnoimprimir.component';
import { TurnodetalleComponent } from './gestion/turno/turnodetalle/turnodetalle.component';
import { TurnoeditarComponent } from './gestion/turno/turnoeditar/turnoeditar.component';
import { ChoferComponent} from './gestion/chofer/chofer.component';
import { AuthGuard } from './gestion/users/auth.guard';
import { GestionComponent } from './gestion/gestion.component';
import { ListadoventaComponent } from './gestion/venta/listadoventa/listadoventa.component';
import { VentadetalleComponent } from './gestion/venta/ventadetalle/ventadetalle.component';
import { EntregaComponent } from './gestion/entrega/entrega.component';
import { ListadoComponent } from './gestion/entrega/listado/listado.component';
import { DetalleComponent } from './gestion/entrega/detalle/detalle.component';
import { ListadoclienteComponent } from './gestion/cliente/listadocliente/listadocliente.component';
import { ReciboComponent } from './gestion/recibo/recibo.component';
import { ListadoreciboComponent } from './gestion/recibo/listadorecibo/listadorecibo.component';
import { DetallereciboComponent } from './gestion/recibo/detallerecibo/detallerecibo.component';
import { ResumenComponent } from './gestion/resumen/resumen.component';
import { ResumendeudaComponent } from './gestion/resumendeuda/resumendeuda.component';
import { ClienteComponent } from './gestion/cliente/cliente.component';
import { TurnocrearComponent } from './gestion/turno/turnocrear/turnocrear.component';
import { DeudapalletCrearComponent } from './gestion/pallet/deudapallet/deudapallet-crear/deudapallet-crear.component';
import { RecibopalletCrearComponent } from './gestion/pallet/recibopallet/recibopallet-crear/recibopallet-crear.component';
import { ResumenpalletComponent } from './gestion/pallet/resumenpallet/resumenpallet.component';
import { ChofereditarComponent } from './gestion/chofer/chofereditar/chofereditar.component';


const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full',  canActivate: [AuthGuard]},
  { path: 'gestion', component: GestionComponent, canActivate: [AuthGuard] },
  { path: 'gestion/productos', component: ProductoComponent, canActivate: [AuthGuard]  },
  { path: 'gestion/tipoproductos', component: TipoproductoComponent, canActivate: [AuthGuard]  },
  { path: 'gestion/productos/:id', component: ProductoDetailComponent, canActivate: [AuthGuard]  },
  { path: 'gestion/productos/:id', component: ProductoDetailComponent, canActivate: [AuthGuard]  },
  { path: 'gestion/ventas', component: VentaComponent, canActivate: [AuthGuard]  },
  { path: 'gestion/ventas/:id/:sucursal', component: VentadetalleComponent, canActivate: [AuthGuard]  },
  { path: 'gestion/ventas/listado', component: ListadoventaComponent, canActivate: [AuthGuard]  },
  { path: 'gestion/turnos', component: TurnoComponent, canActivate: [AuthGuard]  },
  { path: 'gestion/turnoscrear', component: TurnocrearComponent, canActivate: [AuthGuard]  },
  { path: 'gestion/turnos/:id/:sucursal', component: TurnodetalleComponent, pathMatch: 'full', canActivate: [AuthGuard]  },
  { path: 'gestion/turnos/:id/:sucursal/imprimir', component: TurnoimprimirComponent, canActivate: [AuthGuard]  },
  { path: 'gestion/turnos/:id/:sucursal/editar', component: TurnoeditarComponent, canActivate: [AuthGuard]  },
  { path: 'gestion/turnosporlote', component: TurnoporloteComponent, pathMatch: 'full', canActivate: [AuthGuard]  },
  { path: 'gestion/chofercrear', component: ChoferComponent, pathMatch: 'full', canActivate: [AuthGuard]  },
  { path: 'gestion/chofereditar', component: ChofereditarComponent, canActivate: [AuthGuard]},
  { path: 'gestion/entregas', component: EntregaComponent, canActivate: [AuthGuard]},
  { path: 'gestion/entregas/turnos/:idturno/:sucturno', component: EntregaComponent, canActivate: [AuthGuard]},
  { path: 'gestion/entregas/:id/:sucursal', component: DetalleComponent, canActivate: [AuthGuard]},
  { path: 'gestion/entregas/:id/:sucursal/vender', component: VentaComponent, canActivate: [AuthGuard]},
  { path: 'gestion/entregas/listado', component: ListadoComponent, canActivate: [AuthGuard]},
  { path: 'gestion/clientes/listado', component: ListadoclienteComponent, canActivate: [AuthGuard]},
  { path: 'gestion/clientes', component: ClienteComponent, canActivate: [AuthGuard]},
  { path: 'gestion/clientes/:idcliente/entregas', component: ListadoComponent, canActivate: [AuthGuard]},
  { path: 'gestion/clientes/:idcliente/ventas', component: ListadoventaComponent, canActivate: [AuthGuard]},
  { path: 'gestion/clientes/:idcliente/recibos', component: ListadoreciboComponent, canActivate: [AuthGuard]},
  { path: 'gestion/recibos', component: ReciboComponent, canActivate: [AuthGuard]  },
  { path: 'gestion/recibos/listado', component: ListadoreciboComponent, canActivate: [AuthGuard]  },
  { path: 'gestion/recibos/:id/:sucursal', component: DetallereciboComponent, canActivate: [AuthGuard]},
  { path: 'gestion/clientes/:idcliente/resumen', component: ResumenComponent, canActivate: [AuthGuard]},
  { path: 'gestion/clientes/:idcliente/resumendeuda', component: ResumendeudaComponent, canActivate: [AuthGuard]},
  { path: 'gestion/clientes/:idcliente/resumenpallet', component: ResumenpalletComponent, canActivate: [AuthGuard]},
  { path: 'gestion/deudapallet/crear', component: DeudapalletCrearComponent, canActivate: [AuthGuard]},
  { path: 'gestion/recibopallet/crear', component: RecibopalletCrearComponent, canActivate: [AuthGuard]}, 
  { path: 'login', component: LoginComponent },

];


@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)]
})

export class AppRoutingModule { }
