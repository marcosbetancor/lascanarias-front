import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-paginador',
  templateUrl: './paginador.component.html',
  styleUrls: ['./paginador.component.css']
})
export class PaginadorComponent implements OnInit {

  @Input() public page: number;
 
  @Input() public totalPages: number;
 
  @Input() public numItems: Number;
 
  @Output() paginaEmitter: EventEmitter<number> =  new EventEmitter();

  public show: any;

  Arr = Array;
 
  constructor() { }
 
  ngOnInit() {
 
  }
 
  shouldShowItem(i: number){

    if(i < (this.page + 3)){
      this.show = i + 1;
    } else if (i == (this.page + 3)){
      this.show = '...';
    } else if(i > (this.totalPages - 3)){
      this.show = i + 1;
    }
  }

  siguiente(){
 
    this.page++;
 
    this.pasarPagina();
 
  }
 
  anterior(){
 
    this.page--;
 
    this.pasarPagina();
 
  }
 
  pasarPagina() {
 
    this.paginaEmitter.emit(this.page);
 
  }

  irAPagina(page: number) {

    this.page = page;
    
    this.pasarPagina();
  }
}
